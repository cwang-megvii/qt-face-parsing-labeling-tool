### A Startup Project for QT GUI Development

#### Prerequisite
- Qt 5.10.1
- VS2015
- CUDA
- OpenCV 3.2
Should be okay in other platforms, but I didn't test it yet.

##### Install QT:
find `qt-opensource-windows-x86-5.10.1.exe` at `\\MegviiNAS\\Software\\qt-opensource-windows-x86-5.10.1.exe`

##### Install CUDA 9.0
find `cuda_9.0.176_win10.exe` at `\\MegviiNAS\\Software\\cuda_9.0.176_win10.exe`
This is for GPU-based SLIC superpixel computation. If you do not want to use GPU, you need to find the CPU-version of SLIC to compute SLIC. Google `SLIC Superpixel`.

##### Install OpenCV 3.2
find `opencv-3.2.0-vc14.exe` at `\\MegviiNAS\\Software\\opencv-3.2.0-vc14.exe`

2. Run CMake to build the project. Then you will get a GUI like this:
![screenshot](./data/screenshot.png)