#ifndef _MAINAPP_H_
#define _MAINAPP_H_

#include "ui_mainwindow.h"
#include <QWheelEvent>
#include "utils.h"

class MainApp : public QMainWindow
{
	Q_OBJECT

public:
	MainApp();
	~MainApp();

public slots:
	void keyPressEvent(QKeyEvent *event) { ui->gfx_view_canvas->keyPressEvent(event); }
	void keyReleaseEvent(QKeyEvent *event) { ui->gfx_view_canvas->keyReleaseEvent(event); }
	bool event(QEvent *e);

	void update_status_bar(int x, int y);
	void update_window_title(const QString& filename);
	void reset_brush_bottons(bool b);
	void open();
	void batch_slic();

private:
	Ui::ARViewer*				ui;
	int key = -1, key_modified = -1;
};

#endif