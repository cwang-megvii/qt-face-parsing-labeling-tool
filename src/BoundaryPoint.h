#ifndef _BOUNDARY_POINT_H_
#define _BOUNDARY_POINT_H_

#include <QColor>
#include <QGraphicsItem>
#include <QtWidgets>

#include "BoundaryLine.h"
#include "boundary.h"

class BoundaryLine;
class Boundary;
class BoundaryPoint : public QGraphicsItem
{
public:
	BoundaryPoint(const QColor &color, int x, Boundary* parent);

	QRectF boundingRect() const;
	void paint(QPainter *painter, const QStyleOptionGraphicsItem *item, QWidget *widget);
	inline int radius() const { return m_radius; }
	inline void set_radius(int r) { m_radius = r; this->update(); }
	inline int id() const { return m_id; }

	BoundaryPoint *prev = NULL, *next = NULL;
	BoundaryLine *prev_line = NULL, *next_line = NULL;

protected:
	void mousePressEvent(QGraphicsSceneMouseEvent *event);
	void mouseMoveEvent(QGraphicsSceneMouseEvent *event);
	void mouseReleaseEvent(QGraphicsSceneMouseEvent *event);

private:
	void update_lines();

private:
	int m_id;
	QColor color;
	QVector<QPointF> stuff;
	int m_radius = 5;

	QPointF offset = QPointF(m_radius, m_radius);
	Boundary* boundary = NULL;
};

#endif
