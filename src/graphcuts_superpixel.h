#ifndef _GRAPH_CUTS_SUPERPIXEL_H_
#define _GRAPH_CUTS_SUPERPIXEL_H_

#include "gco/GCoptimization.h"
#include <opencv/cv.h>
#include <opencv2/opencv.hpp>
#include <unordered_map>
#include <unordered_set>

#include "utils.h"
#include "super_im.h"

class SuperGraphCut
{
public:
	SuperGraphCut(const cv::Mat& _im, const cv::Mat& _mlbmask, const SuperIm& _supim, int _nlb, float _beta_gc);
	~SuperGraphCut() {}

	void run();
	cv::Mat get_result() const { return result; }

private:
	void init();
	void build_gmm();
	void build_data_term();
	void build_smooth_term();
	float nb_cost(int p1, int p2);
	cv::Mat random_select_rows(cv::Mat& sample);
	void visualize_min_dist();
	cv::Mat compute_dist(seg::label_type lb);
	void normalize_min_dist_map(int start_idx, std::vector<cv::Mat>& map);

private:
	cv::Mat im;
	cv::Mat mlbmask;
	cv::Mat result;
	int num_labels;
	std::vector<float> data_term;
	std::vector<float> smooth_term;

	std::vector<cv::Mat> min_dist_map_color;
	std::vector<cv::Mat> min_dist_map_l2;

	const int GMM_NUM_CLUSTERS = 12;
	const float MAX_FLOW_ENERGY = 1e5;
	SuperIm supim;

	float gc_beta = 100.0;
	float gc_alpha = 50.0;
};

#endif
