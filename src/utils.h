#ifndef _UTILS_H_
#define _UTILS_H_

#ifndef NOMINMAX
#define NOMINMAX
#endif

#include "boost/filesystem.hpp"
#include <vector>
#include <string>
#include <unordered_map>
#include <unordered_set>
#include <algorithm>
#include <queue>
#include <stack>

#include <opencv/cv.h>
#include <opencv2/opencv.hpp>

#include <QImage>
#include <QPixmap>

#include <time.h>

#define USE_GPU_SLIC

#ifdef USE_GPU_SLIC
#include "gSLICr_Lib/gSLICr.h"
#endif

namespace fp
{
	std::vector<std::string>		dir(const std::string& path, const std::string& ext);
	void							fileparts(const std::string& filename, std::string& path, std::string& name, std::string& ext);
	bool							mkdir(const std::string& path);
	std::string						build_path(std::vector<std::string>& parts);
	void							move(const std::string& from, const std::string& to);
}

namespace cvp
{
	cv::Mat							merge_to_bgra(const cv::Mat& bgr, const cv::Mat& alpha);
	void							split_to_bgr_and_a(const cv::Mat & bgra, cv::Mat & bgr, cv::Mat & alpha);
	cv::Mat							im_chess(cv::Size sz);
	QImage							mat_to_qimage(const cv::Mat& inMat, bool inCloneImageData = false);
	QPixmap							mat_to_qpixmap(const cv::Mat& inMat, bool inCloneImageData = false);
	cv::Mat							qimage_to_mat(const QImage &inImage, bool inCloneImageData = false);
	cv::Mat							qpixmap_to_mat(const QPixmap &inPixmap, bool inCloneImageData = false);

	inline QPointF					to_QPoint(const cv::Point& pt) { return QPointF(pt.x, pt.y); }
	inline cv::Point				to_CVPoint(const QPointF& pt) { return cv::Point(pt.x(), pt.y()); }
	inline QColor					to_QColor(const cv::Scalar& c) { return QColor(c[0], c[1], c[2]); }
	inline cv::Scalar				to_CVScalar(const QColor& c) { return cv::Scalar(c.blue(), c.green(), c.red()); }

#ifdef USE_GPU_SLIC
	void							mat_to_slic_uchar4(const cv::Mat& inimg, gSLICr::UChar4Image* outimg);
	int								slic_int_to_mat(const gSLICr::IntImage* inimg, cv::Mat& outimg);
#endif
	cv::Mat							visualize_superpixel(const cv::Mat& sp);

	void							remove_small_region(cv::Mat& rst, int tol);
}

namespace utils
{
	inline double					time() { return (double)clock() / CLOCKS_PER_SEC; }
}

#define	SAFE_DELETE(ptr) if (ptr) { delete ptr; ptr = NULL; }
#define	SAFE_DELETE_ARRAY(ptr) if (ptr) { delete [] ptr; ptr = NULL; }

#endif