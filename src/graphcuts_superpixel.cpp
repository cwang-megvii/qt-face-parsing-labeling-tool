#include "graphcuts_superpixel.h"
#include <algorithm>
#include <fstream>

using namespace std;
using namespace cv;

SuperGraphCut::SuperGraphCut(const cv::Mat& _im, const cv::Mat& _mlbmask, 
	const SuperIm& _supim, int _nlb, float _beta_gc) : im(_im), mlbmask(_mlbmask),
	supim(_supim), num_labels(_nlb), gc_beta(_beta_gc)
{
	cout << "data term(alpha): " << gc_alpha << " smooth term(beta): " << gc_beta << endl;
	double t1, t2;
	init();
	t1 = utils::time();
	build_gmm();
	t2 = utils::time();
	cout << "build_gmm: " << t2 - t1 << " s" << endl;

	build_data_term();
	build_smooth_term();
}

void SuperGraphCut::init()
{
	supim.set_mlbmask(mlbmask);
}

void SuperGraphCut::build_gmm()
{
	Vec3b* imptr = im.ptr<Vec3b>(0);
	// Collect Samples: sample_map: lb => Mat sample(full size), int count, |SKIN|HAIR|CLOTH|
	double t1, t2, t3, t4;
	vector<int> cnts(seg::NUM_LABEL_TYPE, 0);
	vector<Mat> sample_map(seg::NUM_LABEL_TYPE);
	std::vector<cv::Ptr<cv::ml::EM>> em_map(seg::NUM_LABEL_TYPE);
	t1 = utils::time();

	int lb_start = seg::BGD + 1;

	for (int i = 0; i < mlbmask.total(); i++)
	{
		int lb = mlbmask.data[i];
		if (lb == seg::FGD_UKN || (lb == seg::BGD && lb_start == seg::BGD + 1))
			continue;
		if (sample_map[lb].empty())
			sample_map[lb] = Mat(mlbmask.total(), 3, CV_32FC1, Scalar(0.0));
		sample_map[lb].at<Vec3f>(cnts[lb], 0) = imptr[i];
		cnts[lb]++;
	}
	for (int lb = lb_start; lb < seg::NUM_LABEL_TYPE; lb++)
	{
		sample_map[lb] = sample_map[lb].rowRange(0, cnts[lb]);
		cout << "lb: " << lb << " " << sample_map[lb].rows << endl;
	}
	t2 = utils::time();
	cout << "sample collect: " << t2 - t1 << endl;
	// Train on FGD_KWN
	t1 = utils::time();
	for (int lb = lb_start; lb < seg::NUM_LABEL_TYPE; lb++)
	{
		Ptr<ml::EM> p = em_map[lb] = ml::EM::create();
		p->setClustersNumber(GMM_NUM_CLUSTERS);
		p->setCovarianceMatrixType(ml::EM::COV_MAT_SPHERICAL);
		p->setTermCriteria(TermCriteria(TermCriteria::COUNT + TermCriteria::EPS, 300, 0.1));
		Mat _sample = random_select_rows(sample_map[lb]);
		p->trainEM(_sample);
	}
	t2 = utils::time();
	cout << "TrainEM: " << t2 - t1 << endl;

	t1 = utils::time();
	Mat sample_fgd_ukn = supim.get_suptb_color_ukn();
	min_dist_map_color.resize(seg::NUM_LABEL_TYPE);
	min_dist_map_l2.resize(seg::NUM_LABEL_TYPE);
	for (int lb = lb_start; lb < seg::NUM_LABEL_TYPE; lb++)
	{
		Mat dist = compute_dist((seg::label_type)lb);
		//Mat visdist;
		//normalize(dist, visdist, 0, 1, NORM_MINMAX);
		//visdist.convertTo(visdist, CV_8UC1, 255.0);
		//imwrite("dist" + to_string(lb) + ".png", visdist);
		Ptr<ml::EM> p = em_map[lb];
		Mat m = p->getMeans();
		//cout << m << endl;
		min_dist_map_color[lb] = Mat::zeros(sample_fgd_ukn.rows, 1, CV_32FC1);
		min_dist_map_l2[lb] = Mat::zeros(sample_fgd_ukn.rows, 1, CV_32FC1);
		for (int k = 0; k < sample_fgd_ukn.rows; k++)
		{
			Vec3d c = sample_fgd_ukn.ptr<Vec3f>(k)[0];
			double minVal = numeric_limits<double>::max();
			for (int i = 0; i < m.rows; i++)
			{
				Vec3d diff = m.ptr<Vec3d>(i)[0] - c;
				minVal = min(minVal, diff.dot(diff));
			}
			int glbidx = supim.ukn_global_index(k);
			Point2f pxy = supim.get_sup_pos(glbidx);
			float d = dist.at<float>(pxy);
			min_dist_map_color[lb].at<float>(k) = minVal;
			min_dist_map_l2[lb].at<float>(k) = d;
		}
	}
	t2 = utils::time();

	if (1)
	{
		normalize_min_dist_map(lb_start, min_dist_map_color);
		normalize_min_dist_map(lb_start, min_dist_map_l2);
		for (int lb = lb_start; lb < seg::NUM_LABEL_TYPE; lb++)
			min_dist_map_color[lb] = min(min_dist_map_color[lb], min_dist_map_l2[lb]);
	}
	else
	{
		normalize_min_dist_map(lb_start, min_dist_map_color);
	}
	t3 = utils::time();
	cout << "compute min dist: " << t2 - t1 << endl;
	cout << "compute sum: " << t3 - t2 << endl;
	visualize_min_dist();
}

void SuperGraphCut::build_data_term()
{
	int num_superpixels = supim.get_num_superpixels();
	data_term.clear(); data_term.resize(num_superpixels * num_labels, 0.0);
	int cnt = 0;
	for (int i = 0; i < num_superpixels; i++)
	{
		seg::label_type lb = supim.label_at(i);
		for (int l = 0; l < num_labels; l++)
		{
			int idx = i * num_labels + l;
			if (lb == seg::FGD_UKN)
				data_term[idx] = l == 0 ? MAX_FLOW_ENERGY : gc_alpha * min_dist_map_color[l].at<float>(cnt); //l == 0 ? MAX_FLOW_ENERGY : 
			else if (lb == l)
				data_term[idx] = 0.0;
			else
				data_term[idx] = MAX_FLOW_ENERGY;
		}
		if (lb == seg::FGD_UKN)
			cnt++;
	}
}

void SuperGraphCut::build_smooth_term()
{
	smooth_term.clear(); smooth_term.resize(num_labels * num_labels, 1.0);
	for (int l1 = 0; l1 < num_labels; l1++)
	{
		for (int l2 = 0; l2 < num_labels; l2++)
		{
			if (l1 == l2)
				smooth_term[l1 * num_labels + l2] = 0.0;
			else if ((l1 == seg::HAIR && l2 == seg::CLOTH) || (l1 == seg::CLOTH && l2 == seg::HAIR))
				smooth_term[l1 * num_labels + l2] = 1.0;
			else
				smooth_term[l1 * num_labels + l2] = 1.0;
		}
	}
}

float SuperGraphCut::nb_cost(int p1, int p2)
{
	Vec3f c1 = supim.color_at(p1);
	Vec3f c2 = supim.color_at(p2);
	Vec3f diff = c1 - c2;
	return gc_beta / (diff.dot(diff) + 1.0);
}

cv::Mat SuperGraphCut::random_select_rows(cv::Mat& sample)
{
	float t1, t2;
	t1 = utils::time();
	const int MAX_SAMPLES = 1000;
	int nRows = sample.rows;
	if (nRows <= MAX_SAMPLES)
		return sample;

	vector<int> indices;
	if (nRows > MAX_SAMPLES * 100)
	{
		indices.resize(MAX_SAMPLES * 10);
		int interval = nRows / indices.size();
		for (int i = 0; i < indices.size(); i++)
			indices[i] = min(i * interval, nRows);
	}
	else
	{
		indices.resize(nRows);
		for (int i = 0; i < indices.size(); i++)
			indices[i] = i;
	}
	//cout << "indices done" << endl;
	std::random_shuffle(indices.begin(), indices.end());
	//cout << "random_shuffle done" << endl;
	Vec3f* p = sample.ptr<Vec3f>(0);
	Mat out(MAX_SAMPLES, 3, CV_32FC1, Scalar(0.0));
	Vec3f* q = out.ptr<Vec3f>(0);
	for (int i = 0; i < MAX_SAMPLES; i++)
		q[i] = p[indices[i]];
	//std::random_shuffle(p, p + nRows);
	t2 = utils::time();
	//cout << "sample: " << t2 - t1 << endl;
	return out;//sample.rowRange(0, MAX_SAMPLES);
}

void SuperGraphCut::visualize_min_dist()
{
	cout << "visualize_min_dist" << endl;
	Mat vis(im.size(), CV_32FC4, Scalar::all(1.0));
	for (int i = 0; i < vis.rows; i++)
	{
		Vec4f *p1 = vis.ptr<Vec4f>(i);
		for (int j = 0; j < vis.cols; j++)
		{
			int idx = supim.index_at(i, j);
			if (supim.label_at(idx) == seg::FGD_UKN)
			{
				int outidx = supim.ukn_local_index(idx);
				//p1[j][0] = min_dist_map[seg::BGD].at<float>(outidx);
				p1[j][1] = min_dist_map_color[seg::SKIN].at<float>(outidx);
				p1[j][2] = min_dist_map_color[seg::HAIR].at<float>(outidx);
				p1[j][3] = min_dist_map_color[seg::CLOTH].at<float>(outidx);
			}
		}
		//cout << i << endl;
	}
	cout << "visualize_min_dist 2" << endl;
	vis = 255.0 * (1.0 - vis);
	vector<Mat> out;
	split(vis, out);
	for (auto& x : out)
		x.convertTo(x, CV_8UC1);
	imwrite("y.png", out[0]);
	imwrite("b.png", out[1]);
	imwrite("g.png", out[2]);
	imwrite("r.png", out[3]);
}

cv::Mat SuperGraphCut::compute_dist(seg::label_type lb)
{
	Mat a1 = mlbmask == lb, a2;
	Mat knl = getStructuringElement(MORPH_ELLIPSE, Size(3, 3));
	erode(a1, a2, knl);
	a1 -= a2;
	Mat dist;
	distanceTransform(255 - a1, dist, CV_DIST_L2, 3);
	//Mat visdist;
	//normalize(dist, visdist, 0, 1, NORM_MINMAX);
	//imshow("visdist", visdist);
	//waitKey(0);
	return dist;
}

void SuperGraphCut::normalize_min_dist_map(int start_idx, std::vector<cv::Mat>& map)
{
	Mat SumDist = Mat::zeros(map[start_idx].rows, 1, CV_32FC1);
	for (int lb = start_idx; lb < seg::NUM_LABEL_TYPE; lb++)
		SumDist += map[lb];
	for (int lb = start_idx; lb < seg::NUM_LABEL_TYPE; lb++)
		map[lb] /= (SumDist + 1e-5);
}

void SuperGraphCut::run()
{
	cout << "Running..." << endl;
	int num_superpixels = supim.get_num_superpixels();
	cout << "num of nodes: " << num_superpixels << endl;
	GCoptimizationGeneralGraph* gc = new GCoptimizationGeneralGraph(num_superpixels, num_labels);
	for (int i = 0; i < num_superpixels; i++)
		gc->setLabel(i, supim.label_at(i) == seg::FGD_UKN ? seg::SKIN : supim.label_at(i));
	gc->setDataCost(data_term.data());
	gc->setSmoothCost(smooth_term.data());
	Mat superpixel = supim.get_supim_index();
	vector<unordered_map<int, int>> sparse_mat(num_superpixels);
	for (int i = 0; i < superpixel.rows - 1; i++)
	{
		int *p = superpixel.ptr<int>(i);
		for (int j = 0; j < superpixel.cols - 1; j++)
		{
			int p0 = p[j], pr = p[j + 1], pd = p[j + superpixel.cols];
			vector<int> p1 = { p[j + 1], p[j + superpixel.cols] };
			for (int k = 0; k < p1.size(); k++)
			{
				int ridx = min(p0, p1[k]), cidx = max(p0, p1[k]);
				if (p0 != p1[k])
					sparse_mat[ridx][cidx]++;
			}
		}
	}
	for (int i = 0; i < superpixel.rows - 1; i++)
	{
		int* p = superpixel.ptr<int>(i);
		for (int j = 0; j < superpixel.cols - 1; j++)
		{
			int p0 = p[j];
			vector<int> p1 = { p[j + 1], p[j + superpixel.cols] };
			for (int k = 0; k < p1.size(); k++)
			{
				int ridx = min(p0, p1[k]), cidx = max(p0, p1[k]);
				if (p0 != p1[k] && sparse_mat[ridx].count(cidx))
				{
					gc->setNeighbors(ridx, cidx, nb_cost(p0, p1[k]) * sparse_mat[ridx][cidx]);
					sparse_mat[ridx].erase(cidx);
				}
			}
		}
	}
	cout << "Before: " << gc->compute_energy() << endl;
	gc->expansion(2);
	cout << "After:  " << gc->compute_energy() << endl;
	result = Mat::zeros(mlbmask.size(), CV_8UC1);
	int *q = superpixel.ptr<int>(0);
	for (int i = 0; i < result.total(); i++)
		result.data[i] = gc->whatLabel(q[i]);
	delete gc;
}