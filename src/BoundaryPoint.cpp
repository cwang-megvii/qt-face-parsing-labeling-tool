#include "BoundaryPoint.h"
#include <iostream>
#include <algorithm>
using namespace std;

BoundaryPoint::BoundaryPoint(const QColor &color, int x, Boundary* parent)
{
	this->m_id = x;
	this->color = color;
	boundary = parent;

	setFlags(ItemIsSelectable | ItemIsMovable); // ItemIgnoresTransformations
	setAcceptHoverEvents(true);
	setCursor(Qt::OpenHandCursor);
}

QRectF BoundaryPoint::boundingRect() const
{
	return QRectF(QPointF(-m_radius - 2, -m_radius - 2), QPointF(m_radius + 2, m_radius + 2));
}

void BoundaryPoint::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
	Q_UNUSED(widget);

	QTransform t = painter->transform();
	qreal m11 = t.m11(), m22 = t.m22();
	painter->save(); // save painter state
	painter->setTransform(QTransform(
		1, t.m12(), t.m13(), 
		t.m21(), 1, t.m23(), t.m31(),
		t.m32(), t.m33()));

	QColor fillColor = (option->state & QStyle::State_Selected) ? color.dark(150) : color;
	if (option->state & QStyle::State_MouseOver)
		fillColor = fillColor.light(125);

	QPen oldPen = painter->pen();
	QPen pen = oldPen;
	int width = 2;
	if (option->state & QStyle::State_Selected)
		width += 2;

	pen.setWidth(width);
	pen.setColor(QColor(255, 255, 255, 192));
	QBrush b(QColor(255, 0, 0));//painter->brush();
	painter->setBrush(QBrush(fillColor.dark(option->state & QStyle::State_Sunken ? 120 : 100)));
	painter->setPen(pen);
	painter->drawEllipse(QRectF(-m_radius, -m_radius, 2 * m_radius, 2 * m_radius));

	// Draw text
	//int fontsize = 14;
	//QFont font("Consolas", fontsize);
	//font.setStyleStrategy(QFont::ForceOutline);
	//painter->setFont(font);
	//painter->save();//
	//painter->drawText(m_radius - fontsize * 3 / 4, m_radius + fontsize / 2, QString("%1").arg(m_id, 2, 10, QLatin1Char('0')));
	painter->restore();
}

void BoundaryPoint::mousePressEvent(QGraphicsSceneMouseEvent *event)
{
	if (event->button() == Qt::RightButton && boundary->point_at(0) == this)
	{
		boundary->close_boundary();
		return;
	}
	setCursor(Qt::ClosedHandCursor);
	QGraphicsItem::mousePressEvent(event);
	update();
	update_lines();
}

void BoundaryPoint::mouseMoveEvent(QGraphicsSceneMouseEvent *event)
{
	QGraphicsItem::mouseMoveEvent(event);
	if (!boundary->rect().contains(this->pos()))
	{
		float x = boundary->rect().x();
		float y = boundary->rect().y();
		float w = boundary->rect().width();
		float h = boundary->rect().height();

		float px = this->pos().x();
		float py = this->pos().y();

		px = max(min(px, x + w), x);
		py = max(min(py, y + h), y);
		setPos(px, py);
	}
	update();
	update_lines();
}

void BoundaryPoint::mouseReleaseEvent(QGraphicsSceneMouseEvent *event)
{
	cout << "here" << endl;
	QGraphicsItem::mouseReleaseEvent(event);
	if (boundary->is_closed())
	{
		boundary->fill_poly();
	}
	if (!boundary->rect().contains(this->pos()))
	{
		float x = boundary->rect().x();
		float y = boundary->rect().y();
		float w = boundary->rect().width();
		float h = boundary->rect().height();

		float px = this->pos().x();
		float py = this->pos().y();

		px = max(min(px, x + w), x);
		py = max(min(py, y + h), y);
		setPos(px, py);
		cout << "boundary" << endl;
	}
	setCursor(Qt::OpenHandCursor);
	update();
	update_lines();
}

void BoundaryPoint::update_lines()
{
	if (prev_line) 
		prev_line->update(); 
	if (next_line) 
		next_line->update();
}
