#ifndef _BOUNDARY_LINE_H_
#define _BOUNDARY_LINE_H_

#include <QColor>
#include <QGraphicsItem>
#include <QtWidgets>
#include "BoundaryPoint.h"
#include "boundary.h"

class BoundaryPoint;
class Boundary;
class BoundaryLine : public QGraphicsItem
{
public:
	BoundaryLine(const QColor &color, BoundaryPoint* _p1, BoundaryPoint* _p2, Boundary* b);

	QRectF boundingRect() const;
	void paint(QPainter *painter, const QStyleOptionGraphicsItem *item, QWidget *widget);

	BoundaryPoint *p1 = NULL, *p2 = NULL;

private:
	QColor color;
	Boundary* boundary = NULL;
	int init_width = 2;
};
#endif
