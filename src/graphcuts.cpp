#include "graphcuts.h"
#include "im.h"

using namespace std;
using namespace cv;

GraphCut::GraphCut(cv::Mat _im, cv::Mat _init_result, int _nlb, float _beta_gc) 
	: im(_im), init_result(_init_result), num_labels(_nlb), gc_beta(_beta_gc)
{
	init();
	build_gmm();
	build_data_term();
	build_smooth_term();
}

void GraphCut::init()
{
	Mat kernel = (Mat_<float>(3, 3) << 1, 1, 1, 1, -8, 1, 1, 1, 1);
	Mat temp, dist;
	filter2D(init_result, temp, CV_32F, kernel);
	temp.convertTo(temp, CV_8UC1, 255.0);

	distanceTransform(255 - temp, dist, CV_DIST_L2, 3);

	int thres1(4), thres2(6);
	mlbmask = Mat(im.size(), CV_8UC1, Scalar(seg::NUM_LABEL_TYPE));
	mlbmask.setTo(seg::FGD_UKN, dist < thres1);
	init_result.copyTo(mlbmask, (dist < thres2) & (dist >= thres1));
	mlbmask.setTo(seg::SEED, dist >= thres2);
	imwrite("mlbmask.png", mlbmask);
	waitKey(1);

	node_id_map = Mat(mlbmask.size(), CV_32SC1, Scalar(-1));
	int* p = node_id_map.ptr<int>(0);
	int cnt = 0, cnt_fgd_ukn = 0;
	for (int i = 0; i < mlbmask.total(); i++)
	{
		if (mlbmask.data[i] == seg::SEED || mlbmask.data[i] == seg::NUM_LABEL_TYPE)
			continue;
		if (mlbmask.data[i] == seg::FGD_UKN)
			cnt_fgd_ukn++;
		p[i] = cnt++;
	}

	num_nodes = cnt;
	cout << "num_nodes: " << num_nodes << endl;
	cout << "cnt_fgd_ukn: " << cnt_fgd_ukn << endl;
	
	sample_fgd_ukn = Mat(cnt_fgd_ukn, 3, CV_32FC1, Scalar(0.0));
	idx_fgd_ukn.resize(cnt_fgd_ukn, -1);
	//imshow("vis", vis);
	//imshow("mlbmask-2", mlbmask);
	//waitKey(1);
}

void GraphCut::run()
{
	cout << "Running..." << endl;
	float t1, t2, t3, t4;
	t1 = utils::time();
	GCoptimizationGeneralGraph* gc = new GCoptimizationGeneralGraph(num_nodes, num_labels);
	int cnt_node_idx = 0;
	for (int i = 0; i < mlbmask.total(); i++)
	{
		seg::label_type lb = (seg::label_type)mlbmask.data[i];
		if (lb != seg::SEED && lb != seg::NUM_LABEL_TYPE)
			gc->setLabel(cnt_node_idx++, lb == seg::FGD_UKN ? seg::SKIN : lb);
	}
	gc->setDataCost(data_term.data());
	gc->setSmoothCost(smooth_term.data());
	for (int i = 0; i < mlbmask.rows - 1; i++)
	{
		uchar *p0 = mlbmask.ptr(i), *p1 = mlbmask.ptr(i + 1);
		int *pidx0 = node_id_map.ptr<int>(i), *pidx1 = node_id_map.ptr<int>(i + 1);
		for (int j = 0; j < mlbmask.cols - 1; j++)
		{
			if (p0[j] == seg::NUM_LABEL_TYPE || p0[j] == seg::SEED)
				continue;

			seg::label_type pr = (seg::label_type)p0[j + 1];
			seg::label_type pd = (seg::label_type)p1[j];

			int p0idx = pidx0[j], pridx = pidx0[j + 1], pdidx = pidx1[j];
			if (pr != seg::NUM_LABEL_TYPE && pr != seg::SEED)
				gc->setNeighbors(p0idx, pridx, nb_cost(Point(j, i), Point(j + 1, i)));
			if (pd != seg::NUM_LABEL_TYPE && pd != seg::SEED)
				gc->setNeighbors(p0idx, pdidx, nb_cost(Point(j, i), Point(j, i + 1)));
		}
	}
	t2 = utils::time();
	//cout << "Before: " << gc->compute_energy() << endl;
	gc->expansion(2);
	t3 = utils::time();

	//cout << "After:  " << gc->compute_energy() << endl;
	result = init_result.clone();//Mat::zeros(im.size(), CV_8UC1);//init_result.clone();
	int cnt = 0;
	for (int i = 0; i < result.total(); i++)
	{
		seg::label_type lb = (seg::label_type)mlbmask.data[i];
		if (lb != seg::NUM_LABEL_TYPE && lb != seg::SEED)
			result.data[i] = gc->whatLabel(cnt++);
	}
	t4 = utils::time();
	cout << "run: (t2 - t1): " << t2 - t1 << endl;
	cout << "run: (t3 - t2): " << t3 - t2 << endl;
	cout << "run: (t4 - t3): " << t4 - t3 << endl;
	delete gc;
}

void GraphCut::build_gmm()
{
	Vec3b* imptr = im.ptr<Vec3b>(0);
	// Collect Samples: sample_map: lb => Mat sample(full size), int count, |SKIN|HAIR|CLOTH|
	double t1, t2, t3, t4;
	vector<int> cnts(seg::NUM_LABEL_TYPE, 0);
	vector<Mat> sample_map(seg::NUM_LABEL_TYPE);
	std::vector<cv::Ptr<cv::ml::EM>> em_map(seg::NUM_LABEL_TYPE);
	int idx = 0;
	for (int i = 0; i < mlbmask.total(); i++)
	{
		if (mlbmask.data[i] != seg::SEED)
		{
			if (mlbmask.data[i] == seg::FGD_UKN)
			{
				sample_fgd_ukn.ptr<Vec3f>(idx)[0] = imptr[i];
				idx_fgd_ukn[idx] = i;
				idx++;
			}
			continue;
		}
		int lb = init_result.data[i];
		//if (lb == seg::BGD)
		//	continue;
		if (sample_map[lb].empty())
			sample_map[lb] = Mat(mlbmask.total(), 3, CV_32FC1, Scalar(0.0));
		sample_map[lb].at<Vec3f>(cnts[lb], 0) = imptr[i];
		cnts[lb]++;
	}
	for (int lb = seg::BGD; lb < seg::NUM_LABEL_TYPE; lb++)
		sample_map[lb] = sample_map[lb].rowRange(0, cnts[lb]);
	for (int lb = seg::BGD; lb < seg::NUM_LABEL_TYPE; lb++)
	{
		Ptr<ml::EM> p = em_map[lb] = ml::EM::create();
		p->setClustersNumber(GMM_NUM_CLUSTERS);
		p->setCovarianceMatrixType(ml::EM::COV_MAT_SPHERICAL);
		p->setTermCriteria(TermCriteria(TermCriteria::COUNT + TermCriteria::EPS, 300, 0.1));
		Mat _sample = random_select_rows(sample_map[lb]);
		p->trainEM(_sample);
	}

	min_dist_map_color.resize(seg::NUM_LABEL_TYPE);
	min_dist_map_l2.resize(seg::NUM_LABEL_TYPE);
	for (int lb = seg::BGD; lb < seg::NUM_LABEL_TYPE; lb++)
	{
		Mat dist = compute_dist((seg::label_type)lb);
		float *pdist = dist.ptr<float>(0);
		Ptr<ml::EM> p = em_map[lb];
		Mat m = p->getMeans();
		min_dist_map_color[lb] = Mat::zeros(sample_fgd_ukn.rows, 1, CV_32FC1);
		min_dist_map_l2[lb] = Mat::zeros(sample_fgd_ukn.rows, 1, CV_32FC1);
		for (int k = 0; k < sample_fgd_ukn.rows; k++)
		{
			Vec3d c = sample_fgd_ukn.ptr<Vec3f>(k)[0];
			double minVal = numeric_limits<double>::max();
			for (int i = 0; i < m.rows; i++)
			{
				Vec3d diff = m.ptr<Vec3d>(i)[0] - c;
				minVal = min(minVal, diff.dot(diff));
			}
			float d = pdist[idx_fgd_ukn[k]];
			min_dist_map_color[lb].at<float>(k) = minVal;
			min_dist_map_l2[lb].at<float>(k) = d;
		}
	}
	Mat SumDist = Mat::zeros(sample_fgd_ukn.rows, 1, CV_32FC1);
	for (int lb = seg::BGD; lb < seg::NUM_LABEL_TYPE; lb++)
		SumDist += min_dist_map_color[lb];
	for (int lb = seg::BGD; lb < seg::NUM_LABEL_TYPE; lb++)
		min_dist_map_color[lb] /= (SumDist + 1e-5);
	
	SumDist.setTo(0);
	for (int lb = seg::BGD; lb < seg::NUM_LABEL_TYPE; lb++)
		SumDist += min_dist_map_l2[lb];
	for (int lb = seg::BGD; lb < seg::NUM_LABEL_TYPE; lb++)
		min_dist_map_l2[lb] /= (SumDist + 1e-5);
	//
	for (int lb = seg::BGD; lb < seg::NUM_LABEL_TYPE; lb++)
		min_dist_map_color[lb] += min_dist_map_l2[lb];
	
	SumDist.setTo(0);
	for (int lb = seg::BGD; lb < seg::NUM_LABEL_TYPE; lb++)
		SumDist += min_dist_map_color[lb];
	for (int lb = seg::BGD; lb < seg::NUM_LABEL_TYPE; lb++)
		min_dist_map_color[lb] /= (SumDist + 1e-5);
}

void GraphCut::build_data_term()
{
	data_term.clear(); data_term.resize(num_nodes * num_labels, 0.0);
	int cnt_fgd_ukn = 0, cnt_node_idx = 0;
	for (int i = 0; i < mlbmask.total(); i++)
	{
		seg::label_type lb = (seg::label_type)mlbmask.data[i];
		if (lb != seg::SEED && lb != seg::NUM_LABEL_TYPE)
		{
			for (int l = 0; l < num_labels; l++)
			{
				int idx = cnt_node_idx * num_labels + l;
				if (lb == seg::FGD_UKN)
					data_term[idx] = l == 0 ? MAX_FLOW_ENERGY : gc_alpha * min_dist_map_color[l].at<float>(cnt_fgd_ukn);
				else if (lb == l)
					data_term[idx] = 0.0;
				else
					data_term[idx] = MAX_FLOW_ENERGY;
			}
			cnt_node_idx++;
			if (lb == seg::FGD_UKN)
				cnt_fgd_ukn++;
		}
	}
	cout << "cnt_node_idx: " << cnt_node_idx << endl;
}

void GraphCut::build_smooth_term()
{
	smooth_term.clear(); smooth_term.resize(num_labels * num_labels, 1.0);
	for (int l1 = 0; l1 < num_labels; l1++)
	{
		for (int l2 = 0; l2 < num_labels; l2++)
		{
			if (l1 == l2)
				smooth_term[l1 * num_labels + l2] = 0.0;
			else if ((l1 == seg::HAIR && l2 == seg::CLOTH) || (l1 == seg::CLOTH && l2 == seg::HAIR))
				smooth_term[l1 * num_labels + l2] = 1.0;
			else
				smooth_term[l1 * num_labels + l2] = 1.0;
		}
	}
}

float GraphCut::nb_cost(const cv::Point& p1, const cv::Point& p2)
{
	//Rect r(0, 0, im.cols, im.rows);
	//if (!r.contains(p1) || !r.contains(p2))
	//	cout << "nb_cost error" << endl;

	Vec3f c1 = im.at<Vec3b>(p1);
	Vec3f c2 = im.at<Vec3b>(p2);
	Vec3f diff = c1 - c2;
	return gc_beta / (diff.dot(diff) + 1.0);
}

cv::Mat GraphCut::random_select_rows(cv::Mat& sample)
{
	const int MAX_SAMPLES = 1000;
	int nRows = sample.rows;
	if (nRows <= MAX_SAMPLES)
		return sample;
	Vec3f* p = sample.ptr<Vec3f>(0);
	std::random_shuffle(p, p + nRows);
	return sample.rowRange(0, MAX_SAMPLES);
}

cv::Mat GraphCut::compute_dist(seg::label_type lb)
{
	Mat a1 = (mlbmask == seg::SEED) & (init_result == lb), a2;
	Mat knl = getStructuringElement(MORPH_ELLIPSE, Size(3, 3));
	erode(a1, a2, knl);
	a1 -= a2;
	Mat dist;
	distanceTransform(255 - a1, dist, CV_DIST_L2, 3);
	return dist;
}