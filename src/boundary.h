#ifndef _BOUNDARY_H_
#define _BOUNDARY_H_

#include "BoundaryPoint.h"
#include "BoundaryLine.h"
#include "utils.h"
#include <QGraphicsScene>

class BoundaryPoint;
class BoundaryLine;

class Boundary
{
public:
	Boundary() {}

	void set_scene(QGraphicsScene* _sc) { sc = _sc; }
	void add_point(QPointF pt);
	BoundaryPoint* point_at(int idx) const { return boundary_points[idx]; }
	BoundaryLine* line_at(int idx) const { return boundary_lines[idx]; }
	void close_boundary();
	bool is_closed() const { return boundary_points.size() == boundary_lines.size() && !boundary_points.empty(); }
	QRectF rect() const;
	void fill_poly();
	bool contains(QPointF pt) const { return mask.at<uchar>(cvp::to_CVPoint(pt)); }
	cv::Mat get_mask() const { return mask; }
	void reset();

private:
	std::vector<BoundaryPoint*>	boundary_points;
	std::vector<BoundaryLine*> boundary_lines;
	QGraphicsScene* sc = NULL;
	cv::Mat mask;
};

#endif
