/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created by: Qt User Interface Compiler version 5.10.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenu>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSlider>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QTabWidget>
#include <QtWidgets/QTextBrowser>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>
#include "Viewer.h"

QT_BEGIN_NAMESPACE

class Ui_ARViewer
{
public:
    QAction *action_open;
    QAction *action_batch_slic;
    QWidget *wgt_central;
    QGridLayout *gridLayout_3;
    QTabWidget *tbwgt_display_settings;
    QWidget *wgt_display;
    QGridLayout *gridLayout_2;
    QVBoxLayout *verticalLayout;
    QPushButton *ph_bgd_brush;
    QPushButton *pb_skin_brush;
    QPushButton *pb_hair_brush;
    QPushButton *pb_cloth_brush;
    QHBoxLayout *horizontalLayout;
    QPushButton *pb_fill;
    QPushButton *pb_erase;
    QSpacerItem *vspacer_in_tab_display;
    QWidget *wgt_other;
    QGridLayout *gridLayout;
    QPushButton *pb_capture_photo;
    QSpacerItem *vspacer_in_tab_other;
    QLabel *lb_expo_compensation;
    QSlider *slider_expo_compensation;
    QGridLayout *grid_layout_exit;
    QPushButton *pb_restart;
    QPushButton *pb_clear;
    QTextBrowser *text_brower_show_text;
    Viewer *gfx_view_canvas;
    QPushButton *ph_bdry_edit;
    QMenuBar *menu_bar;
    QMenu *menu_file;
    QStatusBar *status_bar;
    QButtonGroup *buttonGroup;

    void setupUi(QMainWindow *ARViewer)
    {
        if (ARViewer->objectName().isEmpty())
            ARViewer->setObjectName(QStringLiteral("ARViewer"));
        ARViewer->resize(1459, 878);
        QFont font;
        font.setFamily(QStringLiteral("Microsoft YaHei UI"));
        font.setPointSize(8);
        ARViewer->setFont(font);
        action_open = new QAction(ARViewer);
        action_open->setObjectName(QStringLiteral("action_open"));
        action_batch_slic = new QAction(ARViewer);
        action_batch_slic->setObjectName(QStringLiteral("action_batch_slic"));
        wgt_central = new QWidget(ARViewer);
        wgt_central->setObjectName(QStringLiteral("wgt_central"));
        gridLayout_3 = new QGridLayout(wgt_central);
        gridLayout_3->setObjectName(QStringLiteral("gridLayout_3"));
        gridLayout_3->setHorizontalSpacing(1);
        gridLayout_3->setVerticalSpacing(2);
        gridLayout_3->setContentsMargins(-1, -1, -1, 9);
        tbwgt_display_settings = new QTabWidget(wgt_central);
        tbwgt_display_settings->setObjectName(QStringLiteral("tbwgt_display_settings"));
        QSizePolicy sizePolicy(QSizePolicy::Fixed, QSizePolicy::Expanding);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(tbwgt_display_settings->sizePolicy().hasHeightForWidth());
        tbwgt_display_settings->setSizePolicy(sizePolicy);
        tbwgt_display_settings->setMinimumSize(QSize(0, 0));
        tbwgt_display_settings->setMaximumSize(QSize(16777215, 16777215));
        QFont font1;
        font1.setFamily(QStringLiteral("Microsoft YaHei UI"));
        font1.setPointSize(8);
        font1.setKerning(true);
        font1.setStyleStrategy(QFont::PreferDefault);
        tbwgt_display_settings->setFont(font1);
        tbwgt_display_settings->setToolTipDuration(-1);
        tbwgt_display_settings->setLayoutDirection(Qt::LeftToRight);
        tbwgt_display_settings->setTabPosition(QTabWidget::North);
        tbwgt_display_settings->setTabShape(QTabWidget::Rounded);
        wgt_display = new QWidget();
        wgt_display->setObjectName(QStringLiteral("wgt_display"));
        gridLayout_2 = new QGridLayout(wgt_display);
        gridLayout_2->setObjectName(QStringLiteral("gridLayout_2"));
        verticalLayout = new QVBoxLayout();
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        ph_bgd_brush = new QPushButton(wgt_display);
        buttonGroup = new QButtonGroup(ARViewer);
        buttonGroup->setObjectName(QStringLiteral("buttonGroup"));
        buttonGroup->addButton(ph_bgd_brush);
        ph_bgd_brush->setObjectName(QStringLiteral("ph_bgd_brush"));
        ph_bgd_brush->setMinimumSize(QSize(0, 40));
        QFont font2;
        font2.setFamily(QStringLiteral("Helvetica"));
        font2.setPointSize(14);
        ph_bgd_brush->setFont(font2);
        QIcon icon;
        icon.addFile(QStringLiteral("../data/yellow.png"), QSize(), QIcon::Normal, QIcon::Off);
        ph_bgd_brush->setIcon(icon);
        ph_bgd_brush->setCheckable(true);
        ph_bgd_brush->setChecked(true);

        verticalLayout->addWidget(ph_bgd_brush);

        pb_skin_brush = new QPushButton(wgt_display);
        buttonGroup->addButton(pb_skin_brush);
        pb_skin_brush->setObjectName(QStringLiteral("pb_skin_brush"));
        pb_skin_brush->setMinimumSize(QSize(0, 40));
        pb_skin_brush->setFont(font2);
        QIcon icon1;
        icon1.addFile(QStringLiteral("../data/blue.png"), QSize(), QIcon::Normal, QIcon::Off);
        pb_skin_brush->setIcon(icon1);
        pb_skin_brush->setCheckable(true);
        pb_skin_brush->setAutoExclusive(false);

        verticalLayout->addWidget(pb_skin_brush);

        pb_hair_brush = new QPushButton(wgt_display);
        buttonGroup->addButton(pb_hair_brush);
        pb_hair_brush->setObjectName(QStringLiteral("pb_hair_brush"));
        pb_hair_brush->setMinimumSize(QSize(0, 40));
        pb_hair_brush->setFont(font2);
        QIcon icon2;
        icon2.addFile(QStringLiteral("../data/green.png"), QSize(), QIcon::Normal, QIcon::Off);
        pb_hair_brush->setIcon(icon2);
        pb_hair_brush->setCheckable(true);
        pb_hair_brush->setAutoExclusive(false);

        verticalLayout->addWidget(pb_hair_brush);

        pb_cloth_brush = new QPushButton(wgt_display);
        buttonGroup->addButton(pb_cloth_brush);
        pb_cloth_brush->setObjectName(QStringLiteral("pb_cloth_brush"));
        pb_cloth_brush->setMinimumSize(QSize(0, 40));
        pb_cloth_brush->setFont(font2);
        QIcon icon3;
        icon3.addFile(QStringLiteral("../data/red.png"), QSize(), QIcon::Normal, QIcon::Off);
        pb_cloth_brush->setIcon(icon3);
        pb_cloth_brush->setCheckable(true);
        pb_cloth_brush->setAutoExclusive(false);

        verticalLayout->addWidget(pb_cloth_brush);

        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
        pb_fill = new QPushButton(wgt_display);
        pb_fill->setObjectName(QStringLiteral("pb_fill"));
        pb_fill->setMinimumSize(QSize(0, 30));
        pb_fill->setCheckable(true);

        horizontalLayout->addWidget(pb_fill);

        pb_erase = new QPushButton(wgt_display);
        buttonGroup->addButton(pb_erase);
        pb_erase->setObjectName(QStringLiteral("pb_erase"));
        pb_erase->setMinimumSize(QSize(0, 30));
        pb_erase->setCheckable(true);
        pb_erase->setAutoExclusive(false);

        horizontalLayout->addWidget(pb_erase);


        verticalLayout->addLayout(horizontalLayout);


        gridLayout_2->addLayout(verticalLayout, 0, 0, 2, 2);

        vspacer_in_tab_display = new QSpacerItem(20, 50, QSizePolicy::Minimum, QSizePolicy::MinimumExpanding);

        gridLayout_2->addItem(vspacer_in_tab_display, 6, 0, 1, 2);

        tbwgt_display_settings->addTab(wgt_display, QString());
        wgt_other = new QWidget();
        wgt_other->setObjectName(QStringLiteral("wgt_other"));
        gridLayout = new QGridLayout(wgt_other);
        gridLayout->setObjectName(QStringLiteral("gridLayout"));
        pb_capture_photo = new QPushButton(wgt_other);
        pb_capture_photo->setObjectName(QStringLiteral("pb_capture_photo"));
        pb_capture_photo->setEnabled(false);

        gridLayout->addWidget(pb_capture_photo, 0, 0, 1, 1);

        vspacer_in_tab_other = new QSpacerItem(20, 161, QSizePolicy::Minimum, QSizePolicy::Expanding);

        gridLayout->addItem(vspacer_in_tab_other, 1, 0, 1, 1);

        lb_expo_compensation = new QLabel(wgt_other);
        lb_expo_compensation->setObjectName(QStringLiteral("lb_expo_compensation"));

        gridLayout->addWidget(lb_expo_compensation, 2, 0, 1, 1);

        slider_expo_compensation = new QSlider(wgt_other);
        slider_expo_compensation->setObjectName(QStringLiteral("slider_expo_compensation"));
        slider_expo_compensation->setMinimum(-4);
        slider_expo_compensation->setMaximum(4);
        slider_expo_compensation->setPageStep(2);
        slider_expo_compensation->setOrientation(Qt::Horizontal);
        slider_expo_compensation->setTickPosition(QSlider::TicksAbove);

        gridLayout->addWidget(slider_expo_compensation, 3, 0, 1, 1);

        tbwgt_display_settings->addTab(wgt_other, QString());

        gridLayout_3->addWidget(tbwgt_display_settings, 2, 2, 1, 1);

        grid_layout_exit = new QGridLayout();
        grid_layout_exit->setObjectName(QStringLiteral("grid_layout_exit"));
        grid_layout_exit->setSizeConstraint(QLayout::SetDefaultConstraint);
        pb_restart = new QPushButton(wgt_central);
        pb_restart->setObjectName(QStringLiteral("pb_restart"));
        QSizePolicy sizePolicy1(QSizePolicy::Minimum, QSizePolicy::Fixed);
        sizePolicy1.setHorizontalStretch(0);
        sizePolicy1.setVerticalStretch(0);
        sizePolicy1.setHeightForWidth(pb_restart->sizePolicy().hasHeightForWidth());
        pb_restart->setSizePolicy(sizePolicy1);
        pb_restart->setMinimumSize(QSize(0, 40));

        grid_layout_exit->addWidget(pb_restart, 0, 0, 1, 1);

        pb_clear = new QPushButton(wgt_central);
        pb_clear->setObjectName(QStringLiteral("pb_clear"));
        sizePolicy1.setHeightForWidth(pb_clear->sizePolicy().hasHeightForWidth());
        pb_clear->setSizePolicy(sizePolicy1);
        pb_clear->setMinimumSize(QSize(0, 40));

        grid_layout_exit->addWidget(pb_clear, 0, 1, 1, 1);


        gridLayout_3->addLayout(grid_layout_exit, 5, 2, 1, 1);

        text_brower_show_text = new QTextBrowser(wgt_central);
        text_brower_show_text->setObjectName(QStringLiteral("text_brower_show_text"));
        QSizePolicy sizePolicy2(QSizePolicy::Ignored, QSizePolicy::Fixed);
        sizePolicy2.setHorizontalStretch(0);
        sizePolicy2.setVerticalStretch(0);
        sizePolicy2.setHeightForWidth(text_brower_show_text->sizePolicy().hasHeightForWidth());
        text_brower_show_text->setSizePolicy(sizePolicy2);

        gridLayout_3->addWidget(text_brower_show_text, 4, 2, 1, 1);

        gfx_view_canvas = new Viewer(wgt_central);
        gfx_view_canvas->setObjectName(QStringLiteral("gfx_view_canvas"));
        gfx_view_canvas->viewport()->setProperty("cursor", QVariant(QCursor(Qt::CrossCursor)));
        gfx_view_canvas->setMouseTracking(false);
        QBrush brush(QColor(64, 128, 128, 255));
        brush.setStyle(Qt::SolidPattern);
        gfx_view_canvas->setBackgroundBrush(brush);
        QBrush brush1(QColor(255, 170, 255, 255));
        brush1.setStyle(Qt::NoBrush);
        gfx_view_canvas->setForegroundBrush(brush1);
        gfx_view_canvas->setRenderHints(QPainter::Antialiasing|QPainter::TextAntialiasing);
        gfx_view_canvas->setDragMode(QGraphicsView::NoDrag);
        gfx_view_canvas->setCacheMode(QGraphicsView::CacheBackground);
        gfx_view_canvas->setTransformationAnchor(QGraphicsView::AnchorViewCenter);
        gfx_view_canvas->setResizeAnchor(QGraphicsView::AnchorUnderMouse);

        gridLayout_3->addWidget(gfx_view_canvas, 2, 0, 4, 2);

        ph_bdry_edit = new QPushButton(wgt_central);
        ph_bdry_edit->setObjectName(QStringLiteral("ph_bdry_edit"));
        ph_bdry_edit->setMinimumSize(QSize(0, 40));
        ph_bdry_edit->setFont(font2);
        ph_bdry_edit->setCheckable(true);

        gridLayout_3->addWidget(ph_bdry_edit, 3, 2, 1, 1);

        ARViewer->setCentralWidget(wgt_central);
        menu_bar = new QMenuBar(ARViewer);
        menu_bar->setObjectName(QStringLiteral("menu_bar"));
        menu_bar->setGeometry(QRect(0, 0, 1459, 21));
        menu_file = new QMenu(menu_bar);
        menu_file->setObjectName(QStringLiteral("menu_file"));
        ARViewer->setMenuBar(menu_bar);
        status_bar = new QStatusBar(ARViewer);
        status_bar->setObjectName(QStringLiteral("status_bar"));
        status_bar->setSizeGripEnabled(true);
        ARViewer->setStatusBar(status_bar);

        menu_bar->addAction(menu_file->menuAction());
        menu_file->addAction(action_open);
        menu_file->addAction(action_batch_slic);

        retranslateUi(ARViewer);
        QObject::connect(ph_bdry_edit, SIGNAL(toggled(bool)), pb_fill, SLOT(setChecked(bool)));

        tbwgt_display_settings->setCurrentIndex(0);

		connectSignals(ARViewer);
        QMetaObject::connectSlotsByName(ARViewer);
    } // setupUi

    void retranslateUi(QMainWindow *ARViewer)
    {
        ARViewer->setWindowTitle(QApplication::translate("ARViewer", "Camera", nullptr));
        action_open->setText(QApplication::translate("ARViewer", "Open", nullptr));
        action_batch_slic->setText(QApplication::translate("ARViewer", "Batch SLIC", nullptr));
        ph_bgd_brush->setText(QApplication::translate("ARViewer", "BGD", nullptr));
#ifndef QT_NO_SHORTCUT
        ph_bgd_brush->setShortcut(QApplication::translate("ARViewer", "Alt+B", nullptr));
#endif // QT_NO_SHORTCUT
        pb_skin_brush->setText(QApplication::translate("ARViewer", "SKIN", nullptr));
#ifndef QT_NO_SHORTCUT
        pb_skin_brush->setShortcut(QApplication::translate("ARViewer", "Alt+S", nullptr));
#endif // QT_NO_SHORTCUT
        pb_hair_brush->setText(QApplication::translate("ARViewer", "HAIR", nullptr));
#ifndef QT_NO_SHORTCUT
        pb_hair_brush->setShortcut(QApplication::translate("ARViewer", "Alt+H", nullptr));
#endif // QT_NO_SHORTCUT
        pb_cloth_brush->setText(QApplication::translate("ARViewer", "CLOTH", nullptr));
#ifndef QT_NO_SHORTCUT
        pb_cloth_brush->setShortcut(QApplication::translate("ARViewer", "Alt+C", nullptr));
#endif // QT_NO_SHORTCUT
        pb_fill->setText(QApplication::translate("ARViewer", "Fill", nullptr));
#ifndef QT_NO_SHORTCUT
        pb_fill->setShortcut(QApplication::translate("ARViewer", "Alt+F", nullptr));
#endif // QT_NO_SHORTCUT
        pb_erase->setText(QApplication::translate("ARViewer", "Erase", nullptr));
#ifndef QT_NO_SHORTCUT
        pb_erase->setShortcut(QApplication::translate("ARViewer", "Alt+E", nullptr));
#endif // QT_NO_SHORTCUT
        tbwgt_display_settings->setTabText(tbwgt_display_settings->indexOf(wgt_display), QApplication::translate("ARViewer", "Display", nullptr));
        pb_capture_photo->setText(QApplication::translate("ARViewer", "Capture Photo", nullptr));
        lb_expo_compensation->setText(QApplication::translate("ARViewer", "Exposure Compensation:", nullptr));
        tbwgt_display_settings->setTabText(tbwgt_display_settings->indexOf(wgt_other), QApplication::translate("ARViewer", "Other", nullptr));
        pb_restart->setText(QApplication::translate("ARViewer", "Restart", nullptr));
        pb_clear->setText(QApplication::translate("ARViewer", "Clear", nullptr));
        ph_bdry_edit->setText(QApplication::translate("ARViewer", "Boundary Edit", nullptr));
        menu_file->setTitle(QApplication::translate("ARViewer", "File", nullptr));
    } // retranslateUi
	void connectSignals(QMainWindow *ARViewer) // <==================
	{
		QObject::connect(gfx_view_canvas, SIGNAL(send_coord(int, int)), ARViewer, SLOT(update_status_bar(int, int)));
		QObject::connect(gfx_view_canvas, SIGNAL(send_filename(const QString&)), ARViewer, SLOT(update_window_title(const QString&)));
		QObject::connect(action_open, SIGNAL(triggered()), ARViewer, SLOT(open()));
		QObject::connect(action_batch_slic, SIGNAL(triggered()), ARViewer, SLOT(batch_slic()));
		QObject::connect(buttonGroup, SIGNAL(buttonClicked(int)), gfx_view_canvas, SLOT(enable_brush_button(int)));
		QObject::connect(pb_restart, SIGNAL(clicked()), gfx_view_canvas, SLOT(reset()));
		QObject::connect(pb_clear, SIGNAL(clicked()), gfx_view_canvas, SLOT(clear()));
		QObject::connect(pb_fill, SIGNAL(toggled(bool)), gfx_view_canvas, SLOT(enable_fill_button(bool)));
		QObject::connect(ph_bdry_edit, SIGNAL(toggled(bool)), ARViewer, SLOT(reset_brush_bottons(bool)));
	}
};

namespace Ui {
    class ARViewer: public Ui_ARViewer {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
