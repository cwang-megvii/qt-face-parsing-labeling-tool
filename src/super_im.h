#ifndef _SUPER_IM_H_
#define _SUPER_IM_H_

#include <opencv/cv.h>
#include <opencv2/opencv.hpp>
#include <unordered_map>
#include <unordered_set>

#include "utils.h"
#include "basic.h"

class SuperIm
{
public:
	SuperIm(const cv::Mat& _im, const std::string& _fname);
	SuperIm() {}
	~SuperIm() {}

	void compute_superpixel(int num_sps = 1e6);
	void set_mlbmask(const cv::Mat& mlbmask);
	inline cv::Mat get_suptb_color_ukn() const { return suptb_color_ukn; }
	inline cv::Mat get_supim_color() const { return supim_color; }
	inline cv::Mat get_supim_index() const { return supim_index; }
	inline int get_num_superpixels() const { return num_superpixels; }
	inline seg::label_type label_at(int idx) const { return suptb_label[idx]; }
	inline int index_at(int y, int x) const { return supim_index.at<int>(y, x); }
	inline cv::Vec3f color_at(int idx) const { return suptb_color[idx]; }
	inline int ukn_local_index(int glb_idx) const { return ukn_index_map[glb_idx]; }
	inline int ukn_global_index(int lcl_idx) const { return ukn_index_map_reverse[lcl_idx]; }
	cv::Point2f get_sup_pos(int k) const { return suptb_pos[k]; }

private:
	void compute_supim_index(int num_sps);
	void compute_supim_color();
	void compute_suptb_color();
	void compute_suptb_label(const cv::Mat& mlbmask);

private:
	cv::Mat supim_color, supim_index;
	cv::Mat im;
	int num_superpixels = -1;

	std::vector<seg::label_type>					suptb_label; // #N x 1
	std::vector<cv::Vec3f>							suptb_color; // #N x Vec3f
	std::vector<cv::Point2f>						suptb_pos; // #N x Point
	std::vector<cv::Mat>							suptb_color_akn_map; // lb => #N_ x Vec3f
	cv::Mat											suptb_color_ukn;

	std::vector<int>								ukn_index_map;
	std::vector<int>								ukn_index_map_reverse;
	std::string										fname;
};

#endif
