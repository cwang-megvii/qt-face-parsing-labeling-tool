#ifndef _IM_H_
#define _IM_H_

#include <QWidget>

#include "utils.h"
#include <string>
#include <iostream>
#include <unordered_map>
#include <unordered_set>
#include <opencv/cv.h>
#include <opencv2/opencv.hpp>

#include "super_im.h"
#include "graphcuts_superpixel.h"

class ImGroup
{
public:
	ImGroup(const QString& fname_im);
	~ImGroup();

	inline int height() const { return im.rows; }
	inline int width() const { return im.cols; }

	inline std::pair<QColor, QColor> penColor(seg::label_type lb) const { return penColor_map.at(lb); }
	inline bool contains(QPointF pt) const { return cv::Rect(0, 0, im.cols, im.rows).contains(cvp::to_CVPoint(pt)); }

	inline const QPixmap& chessboard() const { return chessboard_pxlmap; }
	inline const QPixmap& blended_im() const { return blended_pxlmap; }
	inline const QPixmap& vis_superpixel() const { return superpixel_pxlmap; }
	const QPixmap strokes() const;

	inline QPixmap result() const { return QPixmap::fromImage(display_paint_device); }

	void draw_line_to(seg::label_type lb, QPointF& lastPoint, const QPointF& endPoint, float penWidth);
	void update();
	void fill_area(cv::Mat mask, seg::label_type lb);

	void reload();
	void clear();
	bool save_mlbmask() const;

	void flood_fill(QPointF pt, seg::label_type lb);
	void segment(float gc_beta, cv::Rect update_area);

private:
	QString get_paired_fname(const QString& fname, const std::string& suffix);
	void init(const QString& fname_im, const QString& fname_bmask);
	void render(bool fast = false);
	void compute_superpixels();

	std::unordered_map<seg::label_type, std::pair<QColor, QColor>> penColor_map;

	QPixmap blended_pxlmap, chessboard_pxlmap, superpixel_pxlmap;
	QString fname_mlbmask;
	QString fname_supim;

	QImage virtual_paint_device; // <==> mlbmask, 3-channels, for interaction
	QImage display_paint_device; // only for display

	cv::Mat im, bmask; // store orig im and bmask.
	cv::Mat rst_mlbmask;
	const int NUM_BITS_SHIFT = 5;

	cv::Mat superpixel;
	SuperIm supim;
	bool isSegmented = false;
	bool isEditted = false;

	cv::Size orig_size;
	cv::Size run_size;
	const int MAX_WIDTH_HEIGHT = 800;
};

#endif