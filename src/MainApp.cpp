#include "MainApp.h"
#include <iostream>
#include <QFileDialog>

using namespace std;
using namespace cv;

MainApp::MainApp()
{
	ui = new Ui::ARViewer();
	ui->setupUi(this);
}

MainApp::~MainApp()
{
	SAFE_DELETE(ui);
}

bool MainApp::event(QEvent *e)
{
	if (e->type() == QEvent::WindowActivate)
		ui->gfx_view_canvas->reset_keyboard();
	return QMainWindow::event(e);
}

void MainApp::update_status_bar(int x, int y)
{
	char buff[300];
	sprintf(buff, "(x: %04d, y: %04d)", x, y);
	ui->status_bar->showMessage(QString(buff));
}

void MainApp::open()
{
	QStringList filelist = QFileDialog::getOpenFileNames(this, tr("Open Files"), 
		QDir::currentPath(), tr("JPG (*.jpg);; PNG (*.png);; ALL (*.*)"));
	if (!filelist.isEmpty())
	{
		ui->gfx_view_canvas->set_filelist(filelist);
		ui->gfx_view_canvas->show_image_at(0);
	}
}

void MainApp::batch_slic()
{
	QStringList filelist = QFileDialog::getOpenFileNames(this, tr("Open Files"),
		QDir::currentPath(), tr("JPG (*.jpg);; PNG (*.png);; ALL (*.*)"));
	if (!filelist.isEmpty())
	{
		ui->gfx_view_canvas->set_filelist(filelist);
		for (int i = 0; i < filelist.size(); i++)
			ui->gfx_view_canvas->show_image_at(i);
	}
}

void MainApp::update_window_title(const QString& filename)
{
	this->setWindowTitle(filename);
}

void MainApp::reset_brush_bottons(bool b)
{
	ui->gfx_view_canvas->set_mode(b);
}
