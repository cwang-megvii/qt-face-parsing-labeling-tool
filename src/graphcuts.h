#ifndef _GRAPH_CUTS_H_
#define _GRAPH_CUTS_H_

#include "gco/GCoptimization.h"
#include <opencv/cv.h>
#include <opencv2/opencv.hpp>
#include <unordered_map>
#include "basic.h"

class GraphCut
{
public:
	GraphCut(cv::Mat _im, cv::Mat _init_result, int _nlb, float _beta_gc);
	~GraphCut() {}

	void init();
	void run();
	cv::Mat get_result() const { return result; }

private:
	void build_gmm();
	void build_data_term();
	void build_smooth_term();
	float nb_cost(const cv::Point& p1, const cv::Point& p2);
	cv::Mat random_select_rows(cv::Mat& sample);
	cv::Mat compute_dist(seg::label_type lb);

private:
	cv::Mat im;
	cv::Mat result;
	cv::Mat init_result;
	cv::Mat mlbmask;
	cv::Mat node_id_map;

	cv::Mat sample_fgd_ukn;
	std::vector<int> idx_fgd_ukn;
	int num_labels;
	int num_nodes;

	std::vector<float> data_term;
	std::vector<float> smooth_term;

	std::vector<cv::Mat> min_dist_map_color;
	std::vector<cv::Mat> min_dist_map_l2;

	const int GMM_NUM_CLUSTERS = 12;
	const float MAX_FLOW_ENERGY = 1e5;

	float gc_beta = 100.0;
	float gc_alpha = 50.0;
};

#endif
