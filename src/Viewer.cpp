#include "Viewer.h"
#include "ui_mainwindow.h"

#include <fstream>
#include <QFileInfo>
#include <QDir>
#include <iostream>

using namespace std;
using namespace cv;

Viewer::Viewer(QWidget* parent) : QGraphicsView(parent)
{
	sc = new QGraphicsScene(this);
	this->setScene(sc);
	boundary.set_scene(sc);
}

Viewer::~Viewer()
{
	SAFE_DELETE(sc);
}

void Viewer::wheelEvent(QWheelEvent *event)
{
	if (key_modifiers == (Qt::ControlModifier | Qt::AltModifier))
	{
		if (filelist.empty())
			return;
		const int degrees = event->delta() / 8;
		int steps = degrees / 15;
		if (steps == 0)
			return;
		double m1 = 1.3, m2 = 1.0 / m1;
		double cur_scale = scale_val;
		scale_val = steps > 0 ? min(max_scale, scale_val * m1) : max(min_scale, scale_val * m2);
		m1 = scale_val / cur_scale;
		this->scale(m1, m1);
		penWidth = penWidth / m1;
		//QRectF r = sc->sceneRect();
		//cout << x.x() << " " << x.y() << endl;
		//cout << "QRECT: " << r.x() << " " << r.y() << " " << r.width() << " " << r.height() << endl;
		//get_current_viewing_area();
	}
}

void Viewer::show_image_at(int idx)
{
	if (idx >= filelist.count())
		return;
	QString filename = filelist.at(idx);
	QFileInfo qf(filename);
	if (!qf.exists() || !qf.isFile())
		return;
	SAFE_DELETE(ims);
	ims = new ImGroup(filename);
	img_idx = idx;
	boundary.reset();
	render();
	emit send_filename(filename);
	this->update();
}

void Viewer::save()
{
 	if (img_idx == -1 || ims == NULL)
 		return;
	ims->save_mlbmask();
}

void Viewer::show_superpixel()
{
	pxlmap_mask_item->setPixmap(ims->vis_superpixel());
}

void Viewer::show_strokes()
{
	pxlmap_mask_item->setPixmap(ims->strokes());
}

void Viewer::show_orig_im()
{
	pxlmap_mask_item->setPixmap(ims->blended_im());
}

void Viewer::render()
{
	//resetMatrix();
	//scale_val = 1.0;
	sc->clear();
	sc->addPixmap(ims->chessboard());
	sc->addPixmap(ims->blended_im());
	pxlmap_mask_item = sc->addPixmap(ims->result());
	sc->setSceneRect(ims->blended_im().rect());
}

void Viewer::mousePressEvent(QMouseEvent *event)
{
	if (event->button() == Qt::LeftButton && key_modifiers == (Qt::ControlModifier | Qt::AltModifier))
		this->setDragMode(QGraphicsView::ScrollHandDrag);
	else if (event->button() == Qt::LeftButton && key_modifiers == Qt::ControlModifier)
		this->setDragMode(QGraphicsView::RubberBandDrag);
	else if (event->button() == Qt::LeftButton && ims)
	{
		QPointF pt = mapToScene(event->pos());
		if (!fill_button_enabled && !isBoundaryEdit && ims->contains(pt))
		{
			lastPoint = mapToScene(event->pos());
			update_stroke_area();
			scribbling = true;
		}
	}
	QGraphicsView::mousePressEvent(event);
}

void Viewer::mouseMoveEvent(QMouseEvent *event)
{
	QGraphicsView::mouseMoveEvent(event);
	QPointF pt = mapToScene(event->pos());
	if (event->buttons() == Qt::LeftButton)
	{
		if (scribbling)
		{
			draw_line_to(pt, false);
			update_stroke_area();
			pxlmap_mask_item->setPixmap(ims->result());
		}
		if (isBoundaryEdit)
		{
		}
	}
	if (ims && ims->contains(pt))
		emit send_coord(pt.x(), pt.y());
	this->update();
}

void Viewer::mouseReleaseEvent(QMouseEvent *event)
{
	QPointF pt = mapToScene(event->pos());
	
	if (event->button() == Qt::LeftButton && ims && key_modifiers == -1)
	{
		if (isBoundaryEdit)
		{
			if (!boundary.is_closed() && sc->selectedItems().empty())
			{
				pt.setX(max(0, min((int)pt.x(), ims->width())));
				pt.setY(max(0, min((int)pt.y(), ims->height())));
				boundary.add_point(pt);
			}
			else if (boundary.is_closed() && fill_button_enabled && ims->contains(pt) && boundary.contains(pt))
			{
				cout << "fill boundary" << endl;
				Mat mask = boundary.get_mask();
				ims->fill_area(mask, mLabel);
				boundary.reset();
				//ims->segment(gc_beta);
				pxlmap_mask_item->setPixmap(ims->result());
			}
		}
		else
		{
			if (fill_button_enabled && ims->contains(pt))
			{
				ims->flood_fill(pt, mLabel);
				pxlmap_mask_item->setPixmap(ims->result());
			}
			else if (scribbling)
			{
				draw_line_to(pt, true);
				ims->segment(gc_beta, update_area);//get_current_viewing_area());
				stroke_xmin = stroke_ymin = numeric_limits<float>::max();
				stroke_xmax = stroke_ymax = 0;

				pxlmap_mask_item->setPixmap(ims->result());
				scribbling = false;
			}
		}
	}
	QGraphicsView::mouseReleaseEvent(event);
	setDragMode(QGraphicsView::NoDrag);
}

void Viewer::draw_line_to(const QPointF& endPoint, bool need_update)
{
	ims->draw_line_to(mLabel, lastPoint, endPoint, penWidth);
	lastPoint = endPoint;
	if (need_update)
		ims->update();
}

Rect2f Viewer::get_current_viewing_area() const
{
	QRectF r = this->rect();
	QPoint p1(r.x(), r.y());
	QPoint p2(r.x() + r.width(), r.y() + r.height());
	QPointF p1s = mapToScene(p1), p2s = mapToScene(p2);
	Rect2f r1(p1s.x(), p1s.y(), p2s.x() - p1s.x(), p2s.y() - p1s.y());
	Rect2f r2(0, 0, ims->width(), ims->height());
	Rect2f I = r1 & r2;
	//cout << "Area: " << endl;
	//cout << I.x << " " << I.y << " " << I.width << " " << I.height << endl;
	return I;
}

void Viewer::update_stroke_area()
{
	stroke_xmin = min(stroke_xmin, (float)lastPoint.x());
	stroke_ymin = min(stroke_ymin, (float)lastPoint.y());
	stroke_xmax = max(stroke_xmax, (float)lastPoint.x());
	stroke_ymax = max(stroke_ymax, (float)lastPoint.y());
	float w = stroke_xmax - stroke_xmin, h = stroke_ymax - stroke_ymin;
	float cx = 0.5 * (stroke_xmax + stroke_xmin), cy = 0.5 * (stroke_ymax + stroke_ymin);
	Rect2f r1(cx - 0.5 * w - 2 * penWidth, cy - 0.5 * h - 2 * penWidth, w + 4 * penWidth, h + 4 * penWidth);
	Rect2f r2(0, 0, ims->width(), ims->height());
	update_area = r1 & r2;
}

void Viewer::keyPressEvent(QKeyEvent *event)
{
	key = event->key();
	key_modifiers = event->modifiers();
	if ((key == Qt::Key_D || key == Qt::Key_Right) && img_idx < filelist.count() - 1)
		show_image_at(++img_idx);
	if ((key == Qt::Key_A || key == Qt::Key_Left) && img_idx > 0)
		show_image_at(--img_idx);
	if (key == Qt::Key_S && key_modifiers == Qt::ControlModifier)
		save();
	if (key == Qt::Key_T && key_modifiers == Qt::ControlModifier)
		show_superpixel();
	if (key == Qt::Key_M && key_modifiers == Qt::ControlModifier)
		show_strokes();
	if (key == Qt::Key_R && key_modifiers == Qt::ControlModifier)
		show_orig_im();
	if (key == Qt::Key_Up && key_modifiers == Qt::AltModifier)
	{
		gc_beta *= 1.1;
		ims->segment(gc_beta, update_area);
		pxlmap_mask_item->setPixmap(ims->result());
	}
	if (key == Qt::Key_Down && key_modifiers == Qt::AltModifier)
	{
		gc_beta *= 1.0 / 1.1;
		ims->segment(gc_beta, update_area);
		pxlmap_mask_item->setPixmap(ims->result());
	}
}

void Viewer::keyReleaseEvent(QKeyEvent *event)
{
	reset_keyboard();
	pxlmap_mask_item->setPixmap(ims->result());
}

void Viewer::reset()
{
	if (!ims)
		return;
	ims->reload();
	pxlmap_mask_item->setPixmap(ims->result());
}

void Viewer::clear()
{
	if (!ims)
		return;
	ims->clear();
	pxlmap_mask_item->setPixmap(ims->result());
}

