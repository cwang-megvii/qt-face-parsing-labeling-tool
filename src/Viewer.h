#ifndef _VIEWER_H_
#define _VIEWER_H_

#include <QGraphicsView>
#include <QWidget>
#include <QWheelEvent>
#include <QGraphicsItem>

#include "utils.h"
#include <string>
#include <iostream>
#include <unordered_map>

#include "im.h"
#include "boundary.h"

class Viewer : public QGraphicsView
{
	Q_OBJECT

public:
	Viewer(QWidget* parent);
	~Viewer();

public slots:
	void wheelEvent(QWheelEvent *event);
	void mouseMoveEvent(QMouseEvent *event);
	void mousePressEvent(QMouseEvent *event);
	void mouseReleaseEvent(QMouseEvent *event);
	
	void keyPressEvent(QKeyEvent *event);
	void keyReleaseEvent(QKeyEvent *event);
	
	void enable_brush_button(int idx) { mLabel = button_id_to_lb[idx]; }
	void enable_fill_button(bool b) { fill_button_enabled = b; if (b) scribbling = false; }
	
	void reset();
	void clear();

public:
	inline void set_filelist(const QStringList& lst) { filelist = lst; }
	inline void reset_keyboard() { key = key_modifiers = -1; }
	void show_image_at(int idx);
	void save();
	void show_superpixel();
	void show_strokes();
	void show_orig_im();
	void set_mode(bool b) { isBoundaryEdit = b; if (!b) boundary.reset(); }

Q_SIGNALS:
	void send_coord(int, int);
	void send_filename(const QString&);

private:
	void render();
	void draw_line_to(const QPointF& endPoint, bool need_update = false);
	cv::Rect2f get_current_viewing_area() const;
	void update_stroke_area();

private:
	QGraphicsScene* sc = NULL;
	ImGroup* ims = NULL;
	QGraphicsPixmapItem* pxlmap_mask_item = NULL;
	QGraphicsPathItem* path_item = NULL;

	QStringList filelist;
	int img_idx = -1;

	int x, y;
	double scale_val = 1.0;
	double max_scale = 10.0;
	double min_scale = 0.3;

	int m_orig_x, m_orig_y;

	int key = -1, key_modifiers = -1;

	// Pen:
	bool fill_button_enabled = false;
	bool scribbling = false;
	float penWidth = 50.0;

	std::unordered_map<int, seg::label_type> button_id_to_lb = {
		{-2, seg::BGD},
		{-3, seg::SKIN}, 
		{-4, seg::HAIR}, 
		{-5, seg::CLOTH},
		{-6, seg::FGD_UKN}
	};

	seg::label_type mLabel = seg::BGD; // BGD never needs to be specified.
	QPointF lastPoint;
	float stroke_xmin, stroke_ymin, stroke_xmax, stroke_ymax;
	cv::Rect2f update_area;

	float gc_beta = 11668.2;

	bool isBoundaryEdit = false;
	Boundary boundary;
};

#endif