#include "gmm.h"

using namespace std;
using namespace cv;

GMM::GMM()
{
	const int modelSize = 3/*mean*/ + 9/*covariance*/ + 1/*component weight*/;
	model.create(1, modelSize*componentsCount, CV_32FC1);
	model.setTo(Scalar(0));
	coefs = model.ptr<float>(0);
	mean = coefs + componentsCount;
	cov = mean + 3 * componentsCount;

	for (int ci = 0; ci < componentsCount; ci++)
		if (coefs[ci] > 0)
		{
			bool status = calcInverseCovAndDeterm(ci);
			if (!status)
			{
				cout << "Errors Detected in calcInverseCovAndDeterm. GMM() now return.";
			}
		}
}
GMM::GMM(Mat& _model)
{
	const int modelSize = 3/*mean*/ + 9/*covariance*/ + 1/*component weight*/;
	if (_model.empty())
	{
		_model.create(1, modelSize*componentsCount, CV_32FC1);
		_model.setTo(Scalar(0));
	}
	else if ((_model.type() != CV_32FC1) || (_model.rows != 1) || (_model.cols != modelSize*componentsCount))
		CV_Error(CV_StsBadArg, "_model must have CV_32FC1 type, rows == 1 and cols == 13*componentsCount");

	model = _model;

	coefs = model.ptr<float>(0);
	mean = coefs + componentsCount;
	cov = mean + 3 * componentsCount;

	for (int ci = 0; ci < componentsCount; ci++)
		if (coefs[ci] > 0)
		{
			bool status = calcInverseCovAndDeterm(ci);
			if (!status)
			{
				cout << "Errors Detected in calcInverseCovAndDeterm. GMM(Mat& ) now return." << endl;
			}
		}
}

float GMM::operator()(const Vec3d color) const
{
	float res = 0;
	for (int ci = 0; ci < componentsCount; ci++)
		res += coefs[ci] * (*this)(ci, color);
	return res;
}

float GMM::operator()(int ci, const Vec3d color) const
{
	float res = 0;
	if (coefs[ci] > 0)
	{
		//		CV_Assert( covDeterms[ci] > std::numeric_limits<float>::epsilon() );
		if (covDeterms[ci] <= std::numeric_limits<float>::epsilon())
		{
			// LOGE("Error Return. In operator(): covDeterms Exit numeric limits.");
			return res;
		}
		Vec3d diff = color;
		float* m = mean + 3 * ci;
		diff[0] -= m[0]; diff[1] -= m[1]; diff[2] -= m[2];
		float mult = diff[0] * (diff[0] * inverseCovs[ci][0][0] + diff[1] * inverseCovs[ci][1][0] + diff[2] * inverseCovs[ci][2][0])
			+ diff[1] * (diff[0] * inverseCovs[ci][0][1] + diff[1] * inverseCovs[ci][1][1] + diff[2] * inverseCovs[ci][2][1])
			+ diff[2] * (diff[0] * inverseCovs[ci][0][2] + diff[1] * inverseCovs[ci][1][2] + diff[2] * inverseCovs[ci][2][2]);
		res = 1.0f / sqrt(covDeterms[ci]) * exp(-0.5f*mult);
	}
	return res;
}

int GMM::whichComponent(const Vec3d color) const
{
	int k = 0;
	float max = 0;

	for (int ci = 0; ci < componentsCount; ci++)
	{
		float p = (*this)(ci, color);
		if (p > max)
		{
			k = ci;
			max = p;
		}
	}
	return k;
}

void GMM::initLearning()
{
	for (int ci = 0; ci < componentsCount; ci++)
	{
		sums[ci][0] = sums[ci][1] = sums[ci][2] = 0;
		prods[ci][0][0] = prods[ci][0][1] = prods[ci][0][2] = 0;
		prods[ci][1][0] = prods[ci][1][1] = prods[ci][1][2] = 0;
		prods[ci][2][0] = prods[ci][2][1] = prods[ci][2][2] = 0;
		sampleCounts[ci] = 0;
	}
	totalSampleCount = 0;
}

void GMM::addSample(int ci, const Vec3d color)
{
	sums[ci][0] += color[0]; sums[ci][1] += color[1]; sums[ci][2] += color[2];
	prods[ci][0][0] += color[0] * color[0]; prods[ci][0][1] += color[0] * color[1]; prods[ci][0][2] += color[0] * color[2];
	prods[ci][1][0] += color[1] * color[0]; prods[ci][1][1] += color[1] * color[1]; prods[ci][1][2] += color[1] * color[2];
	prods[ci][2][0] += color[2] * color[0]; prods[ci][2][1] += color[2] * color[1]; prods[ci][2][2] += color[2] * color[2];
	sampleCounts[ci]++;
	totalSampleCount++;
}

bool GMM::endLearning()
{
	const float variance = 0.01;
	for (int ci = 0; ci < componentsCount; ci++)
	{
		int n = sampleCounts[ci];
		if (n == 0)
			coefs[ci] = 0;
		else
		{
			coefs[ci] = (float)n / totalSampleCount;

			float* m = mean + 3 * ci;
			m[0] = sums[ci][0] / n; m[1] = sums[ci][1] / n; m[2] = sums[ci][2] / n;

			float* c = cov + 9 * ci;
			c[0] = prods[ci][0][0] / n - m[0] * m[0]; c[1] = prods[ci][0][1] / n - m[0] * m[1]; c[2] = prods[ci][0][2] / n - m[0] * m[2];
			c[3] = prods[ci][1][0] / n - m[1] * m[0]; c[4] = prods[ci][1][1] / n - m[1] * m[1]; c[5] = prods[ci][1][2] / n - m[1] * m[2];
			c[6] = prods[ci][2][0] / n - m[2] * m[0]; c[7] = prods[ci][2][1] / n - m[2] * m[1]; c[8] = prods[ci][2][2] / n - m[2] * m[2];

			float dtrm = c[0] * (c[4] * c[8] - c[5] * c[7]) - c[1] * (c[3] * c[8] - c[5] * c[6]) + c[2] * (c[3] * c[7] - c[4] * c[6]);
			if (dtrm <= std::numeric_limits<float>::epsilon())
			{
				// Adds the white noise to avoid singular covariance matrix.
				c[0] += variance;
				c[4] += variance;
				c[8] += variance;
			}

			bool status = calcInverseCovAndDeterm(ci);
			if (!status)
			{
				cout << "Error Detected in calcInverseCovAndDeterm(). GMM::endLearning() now return." << endl;
				return false;
			}
		}
	}

	return true;
}

bool GMM::calcInverseCovAndDeterm(int ci)
{
	if (coefs[ci] <= 0)
	{
		cout << "Error Return. In GMM::calcInverseCovAndDeterm: Invalid Entry." << endl;
		return false;
	}
	if (coefs[ci] > 0)
	{
		float *c = cov + 9 * ci;
		float dtrm =
			covDeterms[ci] = c[0] * (c[4] * c[8] - c[5] * c[7]) - c[1] * (c[3] * c[8] - c[5] * c[6]) + c[2] * (c[3] * c[7] - c[4] * c[6]);

		//CV_Assert( dtrm > std::numeric_limits<float>::epsilon() );
		if (dtrm <= std::numeric_limits<float>::epsilon())
		{
			cout << "Error Return. In GMM::calcInverseCovAndDeterm: Exceed Numeric Limits." << endl;
			return false;
		}

		inverseCovs[ci][0][0] = (c[4] * c[8] - c[5] * c[7]) / dtrm;
		inverseCovs[ci][1][0] = -(c[3] * c[8] - c[5] * c[6]) / dtrm;
		inverseCovs[ci][2][0] = (c[3] * c[7] - c[4] * c[6]) / dtrm;
		inverseCovs[ci][0][1] = -(c[1] * c[8] - c[2] * c[7]) / dtrm;
		inverseCovs[ci][1][1] = (c[0] * c[8] - c[2] * c[6]) / dtrm;
		inverseCovs[ci][2][1] = -(c[0] * c[7] - c[1] * c[6]) / dtrm;
		inverseCovs[ci][0][2] = (c[1] * c[5] - c[2] * c[4]) / dtrm;
		inverseCovs[ci][1][2] = -(c[0] * c[5] - c[2] * c[3]) / dtrm;
		inverseCovs[ci][2][2] = (c[0] * c[4] - c[1] * c[3]) / dtrm;

		return true;
	}
}