#ifndef _GMM_H_
#define _GMM_H_

#include <opencv/cv.h>
#include <opencv2/opencv.hpp>

class GMM
{
public:
	static const int componentsCount = 3;
	GMM();

	GMM(cv::Mat& _model);
	float operator()(const cv::Vec3d color) const;
	float operator()(int ci, const cv::Vec3d color) const;
	int whichComponent(const cv::Vec3d color) const;

	void initLearning();
	void addSample(int ci, const cv::Vec3d color);
	bool endLearning();

	cv::Mat model;
	float inverseCovs[componentsCount][3][3];
	float covDeterms[componentsCount];

private:
	bool calcInverseCovAndDeterm(int ci);

	float* coefs;
	float* mean;
	float* cov;

	float sums[componentsCount][3];
	float prods[componentsCount][3][3];
	int sampleCounts[componentsCount];
	int totalSampleCount;
};

#endif
