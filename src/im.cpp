#include "im.h"
#include <QFileInfo>
#include <QDir>
#include <QPainter>

#include "graphcuts.h"
#include "graphcuts_superpixel.h"

using namespace std;
using namespace cv;
using namespace seg;

ImGroup::ImGroup(const QString& fname_im)
{
	penColor_map = {
		// INDEX => virtual_pen_color, display_pen_color
		{ BGD,		{ QColor(BGD, BGD, BGD), Qt::yellow } },
		{ SKIN,		{ QColor(SKIN, SKIN, SKIN), Qt::blue } },
		{ HAIR,		{ QColor(HAIR, HAIR, HAIR), Qt::green } },
		{ CLOTH,	{ QColor(CLOTH, CLOTH, CLOTH), Qt::red } },
		{ FGD_UKN,	{ QColor(FGD_UKN, FGD_UKN, FGD_UKN), Qt::transparent } },
	};
	fname_mlbmask = get_paired_fname(fname_im, ".mlbmask.png");
	fname_supim = get_paired_fname(fname_im, ".spim.png");
	QString fname_bmask = get_paired_fname(fname_im, ".mask.png");
	init(fname_im, fname_bmask);
	reload();
	compute_superpixels();
}

ImGroup::~ImGroup()
{
	save_mlbmask();
}

void ImGroup::init(const QString& fname_im, const QString& fname_bmask)
{
	std::string _fname_im = fname_im.toUtf8().constData();
	std::string _fname_bmask = fname_bmask.toUtf8().constData();
	im = cv::imread(_fname_im, cv::IMREAD_COLOR);
	//GaussianBlur(im, im, Size(5, 5), 15.0);
	bmask = cv::imread(_fname_bmask, cv::IMREAD_GRAYSCALE);
	if (bmask.empty())
		bmask = cv::Mat(im.size(), CV_8UC1, Scalar(FGD_UKN));
	orig_size = im.size();
	if (0 && min(im.rows, im.cols) > MAX_WIDTH_HEIGHT)
	{
		if (im.rows > im.cols)
		{
			int H = MAX_WIDTH_HEIGHT, W = MAX_WIDTH_HEIGHT * im.cols / (float)im.rows;
			run_size = Size(W, H);
			resize(im, im, run_size, 0, 0, INTER_NEAREST);
			resize(bmask, bmask, run_size, 0, 0, INTER_NEAREST);
		}
		else
		{
			int W = MAX_WIDTH_HEIGHT, H = MAX_WIDTH_HEIGHT * im.rows / (float)im.cols;
			run_size = Size(W, H);
			resize(im, im, run_size, 0, 0, INTER_NEAREST);
			resize(bmask, bmask, run_size, 0, 0, INTER_NEAREST);
		}
	}
	chessboard_pxlmap = cvp::mat_to_qpixmap(cvp::im_chess(im.size()), true);
	blended_pxlmap = cvp::mat_to_qpixmap(cvp::merge_to_bgra(im, bmask / 255 * 128 + 127), true);
}

void ImGroup::render(bool fast) // rst_mlbmask ==> display_paint_device
{
	if (display_paint_device.isNull())
		display_paint_device = QImage(virtual_paint_device.size(), QImage::Format_ARGB32);
	if (fast)
	{
		display_paint_device.fill(Qt::transparent);
		return;
	}
	cv::Mat y = cvp::qimage_to_mat(display_paint_device);
	cv::Vec4b* q = (cv::Vec4b*)y.ptr(0);
	for (int i = 0; i < rst_mlbmask.total(); i++)
	{
		label_type lb = (label_type)rst_mlbmask.data[i];
		if (lb == SKIN || lb == HAIR || lb == CLOTH || lb == BGD)
		{
			QColor qc = penColor_map.at(lb).second;
			q[i] = cv::Vec4b(qc.blue(), qc.green(), qc.red(), 127);
		}
		else
			q[i] = cv::Vec4b(0, 0, 0, 0);
	}
}

void ImGroup::compute_superpixels()
{
	supim = SuperIm(im, fname_supim.toUtf8().constData());
	supim.compute_superpixel();
	cout << "num super pixels: " << supim.get_num_superpixels() << endl;
	cout << "num pixels: " << im.total() << endl;
	cout << "super / pp: " << im.total() / (float)supim.get_num_superpixels() << endl;
	superpixel_pxlmap = cvp::mat_to_qpixmap(supim.get_supim_color(), true);
}

QString ImGroup::get_paired_fname(const QString& fname, const std::string& suffix)
{
	std::string temp = fname.toUtf8().constData();
	std::string path, name, ext;
	fp::fileparts(temp, path, name, ext);
	std::string out = fp::build_path(std::vector<std::string>{path, name + suffix});
	return QString::fromStdString(out);
}

bool ImGroup::save_mlbmask() const
{
	if (!isEditted && !isSegmented)
		return false;
	cv::Mat mlbmask = rst_mlbmask.clone();
	for (int i = 0; i < mlbmask.total(); i++)
		mlbmask.data[i] = mlbmask.data[i] == FGD_UKN ? FGD_UKN : (mlbmask.data[i] << NUM_BITS_SHIFT);
	//resize(mlbmask, mlbmask, orig_size, 0, 0, INTER_NEAREST);
	return imwrite(fname_mlbmask.toUtf8().constData(), mlbmask);
}

const QPixmap ImGroup::strokes() const
{
	Mat x = cvp::qimage_to_mat(virtual_paint_device, true);
	cvtColor(x, x, CV_BGR2GRAY);
	for (int i = 0; i < x.total(); i++)
		x.data[i] = x.data[i] == FGD_UKN ? FGD_UKN : (x.data[i] << (NUM_BITS_SHIFT + 1));
	cvtColor(x, x, CV_GRAY2BGR);
	QImage y = cvp::mat_to_qimage(x, true);
	return QPixmap::fromImage(y);
}

void ImGroup::draw_line_to(label_type lb, QPointF& lastPoint, const QPointF& endPoint, float penWidth)
{
	QImage* draw_src[2] = { &virtual_paint_device, &display_paint_device };
	QColor qc[2] = { penColor_map.at(lb).first, penColor_map.at(lb).second };
	float opc[2] = { 1.0, 0.5 };
	for (int i = 0; i < 2; i++)
	{
		QPainter p(draw_src[i]);
		p.setCompositionMode(QPainter::CompositionMode_Source);
		p.setOpacity(opc[i]);
		p.setPen(QPen(qc[i], round(penWidth), Qt::SolidLine, Qt::RoundCap, Qt::RoundJoin));
		if (lastPoint == endPoint)
			p.drawPoint(endPoint);
		else
			p.drawLine(lastPoint, endPoint);
	}
	isEditted = true;
}

void ImGroup::update() // virtual_paint_device ==> mlbmask
{
	cv::Mat v, d, gv;
	v = cvp::qimage_to_mat(virtual_paint_device);
	v.setTo(Scalar::all(BGD), bmask == 0);
	//imshow("virtual", v);

	d = cvp::qimage_to_mat(display_paint_device);
	d.setTo(Scalar::all(BGD), bmask == 0);
	//imshow("display", d);

	//imshow("rst_mlbmask(before)", rst_mlbmask);
	cvtColor(v, gv, COLOR_BGR2GRAY);
	gv.copyTo(rst_mlbmask, gv != seg::FGD_UKN);

	render();
	//rst_mlbmask.setTo(Scalar::all(BGD), bmask == 0);

	//imshow("rst_mlbmask(after)", rst_mlbmask);
	//waitKey(1);
}

void ImGroup::fill_area(cv::Mat mask, seg::label_type lb)
{
	if (isSegmented)
	{
		Mat backup = rst_mlbmask.clone();
		rst_mlbmask.setTo(lb, mask & bmask);
		Mat diff = backup != rst_mlbmask;
		Mat x = cvp::qimage_to_mat(virtual_paint_device);
		x.setTo(Scalar::all(lb), diff);
	}
	else
	{
		Mat x = cvp::qimage_to_mat(virtual_paint_device);
		x.setTo(Scalar::all(lb), mask & bmask);
		rst_mlbmask.setTo(lb, mask & bmask);
	}
	isEditted = true;
	render();
}

void ImGroup::reload()
{
	cv::Mat mlbmask = cv::imread(fname_mlbmask.toUtf8().constData(), IMREAD_GRAYSCALE);
	if (mlbmask.empty())
		mlbmask = bmask.clone();
	//resize(mlbmask, mlbmask, run_size, 0, 0, INTER_NEAREST);
	for (int i = 0; i < mlbmask.total(); i++)
		mlbmask.data[i] = mlbmask.data[i] == FGD_UKN ? FGD_UKN : (mlbmask.data[i] >> NUM_BITS_SHIFT);
	rst_mlbmask = mlbmask;
	cv::cvtColor(mlbmask, mlbmask, COLOR_GRAY2BGR);
	virtual_paint_device = cvp::mat_to_qimage(mlbmask, true);
	render();
	isSegmented = false;
	isEditted = false;
}

void ImGroup::clear()
{
	cv::Mat mlbmask = bmask.clone();
	rst_mlbmask = mlbmask;
	cv::cvtColor(mlbmask, mlbmask, COLOR_GRAY2BGR);
	virtual_paint_device = cvp::mat_to_qimage(mlbmask, true);
	render();
	isSegmented = false;
	isEditted = false;
}

void ImGroup::flood_fill(QPointF pt, label_type lb)
{
	assert(lb != BGD);
	if (bmask.at<uchar>(pt.y(), pt.x()) == seg::BGD)
		return;
	if (isSegmented)
	{
		Mat backup = rst_mlbmask.clone();
		cv::floodFill(rst_mlbmask, cvp::to_CVPoint(pt), lb);
		Mat diff = backup != rst_mlbmask;
		Mat x = cvp::qimage_to_mat(virtual_paint_device);
		x.setTo(Scalar::all(lb), diff);
	}
	else
	{
		Mat x = cvp::qimage_to_mat(virtual_paint_device);
		cv::floodFill(x, cvp::to_CVPoint(pt), Scalar::all(lb));
		cv::floodFill(rst_mlbmask, cvp::to_CVPoint(pt), lb);
	}
	isEditted = true;
	render();
}

void ImGroup::segment(float gc_beta, cv::Rect update_area)
{
	cv::Mat mlbmask, x = cvp::qimage_to_mat(virtual_paint_device);
	cv::cvtColor(x, mlbmask, COLOR_BGR2GRAY);

	unordered_set<label_type> S;
	for (int i = 0; i < mlbmask.total(); i++)
		S.insert((label_type)mlbmask.data[i]);
	bool hasUkn = S.count(FGD_UKN);
	S.erase(FGD_UKN);
	int num_labels = S.size();
	cout << "hasUkn: " << hasUkn << " num_labels: " << num_labels << endl;
	if (!hasUkn || num_labels < NUM_LABEL_TYPE)
		return;

	SuperGraphCut sgc(im, mlbmask, supim, num_labels, gc_beta);
	sgc.run();
	Mat rst = sgc.get_result();

	GraphCut gc(im, rst, num_labels, gc_beta);
	gc.run();
	Mat finalrst = gc.get_result(); // rst.clone();//;

	finalrst.setTo(Scalar(seg::BGD), bmask == BGD);
	mlbmask.copyTo(finalrst, (finalrst != BGD) & (mlbmask != FGD_UKN));

	//medianBlur(finalrst, finalrst, 5);
	imwrite("before.png", finalrst);
	cvp::remove_small_region(finalrst, 10);
	imwrite("after.png", finalrst);

	if (isSegmented)
	{
		finalrst(update_area).copyTo(rst_mlbmask(update_area));
		//finalrst.copyTo(rst_mlbmask);
	}
	else
		finalrst.copyTo(rst_mlbmask);
	render();
	isSegmented = true;
}
