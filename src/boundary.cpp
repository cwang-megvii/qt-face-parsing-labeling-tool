#include "boundary.h"

using namespace std;
using namespace cv;

void Boundary::add_point(QPointF pt)
{
	if (is_closed())
		return;
	BoundaryPoint* bpt = new BoundaryPoint(QColor(0, 0, 0, 127), boundary_points.size(), this);
	bpt->setPos(pt);
	sc->addItem(bpt);
	if (!boundary_points.empty())
	{
		BoundaryPoint* last_bpt = boundary_points.back();
		last_bpt->next = bpt;
		bpt->prev = last_bpt;
		BoundaryLine* bl = new BoundaryLine(Qt::darkBlue, last_bpt, bpt, this);
		boundary_lines.push_back(bl);
		sc->addItem(bl);
		last_bpt->next_line = bl;
		bpt->prev_line = bl;
	}
	boundary_points.push_back(bpt);
}

void Boundary::close_boundary()
{
	if (boundary_points.size() < 3 || is_closed())
		return;
	BoundaryPoint* last_bpt = boundary_points.back();
	BoundaryPoint* bpt = boundary_points.front();
	last_bpt->next = bpt;
	bpt->prev = last_bpt;

	BoundaryLine* bl = new BoundaryLine(Qt::darkBlue, bpt, last_bpt, this);
	boundary_lines.push_back(bl);
	last_bpt->next_line = bl;
	bpt->prev_line = bl;

	sc->addItem(bl);

	QRectF r = sc->sceneRect();
	mask = cv::Mat(r.height(), r.width(), CV_8UC1, Scalar(0));
	fill_poly();
}

QRectF Boundary::rect() const
{
	if (sc)
		return sc->sceneRect();
	else
		return QRectF(0, 0, 0, 0);
}

void Boundary::fill_poly()
{
	mask.setTo(0);
	vector<cv::Point> pts(boundary_points.size());
	for (int i = 0; i < boundary_points.size(); i++)
		pts[i] = Point(boundary_points[i]->x(), boundary_points[i]->y());
	fillPoly(mask, vector<vector<cv::Point>>(1, pts), Scalar(255));
	//Mat vis;
	//resize(mask, vis, Size(), 0.2, 0.2);
	//imshow("x", vis);
	//waitKey(1);
}

void Boundary::reset()
{
	mask.release();
	for (int i = 0; i < boundary_points.size(); i++)
	{
		sc->removeItem(boundary_points[i]);
		SAFE_DELETE(boundary_points[i]);
	}
	for (int i = 0; i < boundary_lines.size(); i++)
	{
		sc->removeItem(boundary_lines[i]);
		SAFE_DELETE(boundary_lines[i]);
	}
	boundary_points.clear();
	boundary_lines.clear();
}
