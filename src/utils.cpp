#include "utils.h"
#include <iostream>
#include <QDebug>
#include <QtGlobal>

using namespace std;
using namespace cv;
namespace fs = boost::filesystem;

std::vector<std::string> fp::dir(const std::string& path, const std::string& ext)
{
	vector<string> filelist;
	fs::path fs_path(path);
	if (fs_path.empty() || !fs::exists(fs_path))
		return filelist;
	fs::directory_iterator end;
	for (fs::directory_iterator i(fs_path); i != end; ++i)
	{
		const fs::path cp = (*i);
		if (fs::is_regular_file(cp) && cp.extension() == ext)
			filelist.push_back(cp.string());
	}
	return filelist;
}

void fp::fileparts(const std::string& filename, std::string& path, std::string& name, std::string& ext)
{
	fs::path fs_path(filename);
	path = fs_path.parent_path().string();
	name = fs_path.stem().string();
	ext = fs_path.extension().string();
}

bool fp::mkdir(const std::string& path)
{
	fs::path cp(path);
	if (!fs::exists(cp))
		return fs::create_directories(cp);
	else
		return false;
}

std::string fp::build_path(std::vector<std::string>& parts)
{
	if (parts.empty())
		return "";
	fs::path cp(parts[0]);
	for (int i = 1; i < parts.size(); i++)
		cp /= fs::path(parts[i]);
	return cp.string();
}

void fp::move(const std::string& from, const std::string& to)
{
	fs::path path_src(from), path_dst(to);
	if (fs::is_directory(path_dst))
	{
		string p, name, ext;
		fp::fileparts(from, p, name, ext);
		path_dst = fp::build_path(vector<string>{to, name + ext});
	}
	fs::rename(path_src, path_dst);
	cout << "moving " << from << " to " << to << endl;
}

cv::Mat cvp::merge_to_bgra(const cv::Mat& bgr, const cv::Mat& alpha)
{
	vector<Mat> chnls;
	cv::split(bgr, chnls);
	chnls.push_back(alpha);
	Mat out;
	cv::merge(chnls, out);
	return out;
}

void cvp::split_to_bgr_and_a(const cv::Mat& bgra, cv::Mat& bgr, cv::Mat& alpha)
{
	vector<Mat> chnls;
	cv::split(bgra, chnls);
	vector<Mat> vbgr(chnls.begin(), chnls.begin() + 3);
	cv::merge(vbgr, bgr);
	alpha = chnls.back();
}

cv::Mat cvp::im_chess(cv::Size sz)
{
	int block_size = 50;
	Mat out(sz, CV_8UC1, Scalar(255));
	for (int i = 0; i < sz.height; i += block_size)
	{
		for (int j = (i % (block_size * 2)); j < sz.width; j += block_size * 2)
		{
			Mat roi = out(Rect(j, i, min(block_size, sz.width - j), min(block_size, sz.height - i)));
			roi.setTo(127);
		}
	}
	return out;
}

QImage cvp::mat_to_qimage(const cv::Mat& inMat, bool inCloneImageData)
{
	unsigned char* _data = inMat.data;
	int _width(inMat.cols), _height(inMat.rows), _step(inMat.step);

	switch (inMat.type())
	{
		// 8-bit, 4 channel
	case CV_8UC4:
	{
		QImage image(_data, _width, _height, static_cast<int>(_step), QImage::Format_ARGB32);
		return inCloneImageData ? image.copy() : image;
	}

	// 8-bit, 3 channel
	case CV_8UC3:
	{
		QImage image(_data, _width, _height, static_cast<int>(_step), QImage::Format_RGB888);
		QImage temp = image.rgbSwapped();
		return inCloneImageData ? temp.copy() : temp;
	}

	// 8-bit, 1 channel
	case CV_8UC1:
	{
#if QT_VERSION >= QT_VERSION_CHECK(5, 5, 0)
		QImage image(_data, _width, _height, static_cast<int>(_step), QImage::Format_Grayscale8);
#else
		static QVector<QRgb>  sColorTable;
		// only create our color table the first time
		if (sColorTable.isEmpty())
		{
			sColorTable.resize(256);
			for (int i = 0; i < 256; ++i)
				sColorTable[i] = qRgb(i, i, i);
		}
		QImage image(_data, _width, _height, static_cast<int>(_step), QImage::Format_Indexed8);
		image.setColorTable(sColorTable);
#endif
		return inCloneImageData ? image.copy() : image;
	}
	default:
		qWarning() << "mat_to_qimage() - cv::Mat image type not handled in switch:" << inMat.type();
		break;
	}

	return QImage();
}

QPixmap cvp::mat_to_qpixmap(const cv::Mat& inMat, bool inCloneImageData)
{
	return QPixmap::fromImage(mat_to_qimage(inMat, inCloneImageData));
}

cv::Mat cvp::qimage_to_mat(const QImage &inImage, bool inCloneImageData /*= true*/)
{
	switch (inImage.format())
	{
		// 8-bit, 4 channel
	case QImage::Format_ARGB32:
	case QImage::Format_ARGB32_Premultiplied:
	{
		cv::Mat  mat(inImage.height(), inImage.width(), CV_8UC4,
			const_cast<uchar*>(inImage.bits()),
			static_cast<size_t>(inImage.bytesPerLine()));
		return (inCloneImageData ? mat.clone() : mat);
	}

	// 8-bit, 3 channel
	case QImage::Format_RGB32:
	{
		if (!inCloneImageData)
			qWarning() << "qimage_to_mat() - Conversion requires cloning so we don't modify the original QImage data";
		cv::Mat  mat(inImage.height(), inImage.width(), CV_8UC4,
			const_cast<uchar*>(inImage.bits()),
			static_cast<size_t>(inImage.bytesPerLine()));
		cv::Mat  matNoAlpha;
		cv::cvtColor(mat, matNoAlpha, cv::COLOR_BGRA2BGR);   // drop the all-white alpha channel
		return matNoAlpha;
	}

	// 8-bit, 3 channel
	case QImage::Format_RGB888:
	{
		//if (!inCloneImageData)
		//	qWarning() << "ASM::QImageToCvMat() - Conversion requires cloning so we don't modify the original QImage data";
		return cv::Mat(inImage.height(), inImage.width(), CV_8UC3,
				const_cast<uchar*>(inImage.bits()),
				static_cast<size_t>(inImage.bytesPerLine()));

		//QImage swapped = inImage.rgbSwapped();
		//return cv::Mat(swapped.height(), swapped.width(), CV_8UC3,
		//	const_cast<uchar*>(swapped.bits()),
		//	static_cast<size_t>(swapped.bytesPerLine())).clone();
	}

	// 8-bit, 1 channel
	case QImage::Format_Indexed8:
	{
		cv::Mat  mat(inImage.height(), inImage.width(), CV_8UC1,
			const_cast<uchar*>(inImage.bits()),
			static_cast<size_t>(inImage.bytesPerLine()));
		return (inCloneImageData ? mat.clone() : mat);
	}

	default:
		qWarning() << "ASM::QImageToCvMat() - QImage format not handled in switch:" << inImage.format();
		break;
	}

	return cv::Mat();
}

cv::Mat cvp::qpixmap_to_mat(const QPixmap &inPixmap, bool inCloneImageData /*= true*/)
{
	return qimage_to_mat(inPixmap.toImage(), inCloneImageData);
}


#ifdef USE_GPU_SLIC
void cvp::mat_to_slic_uchar4(const cv::Mat& inimg, gSLICr::UChar4Image* outimg)
{
	gSLICr::Vector4u* outimg_ptr = outimg->GetData(MEMORYDEVICE_CPU);
	for (int y = 0; y < outimg->noDims.y; y++)
		for (int x = 0; x < outimg->noDims.x; x++)
		{
			int idx = x + y * outimg->noDims.x;
			outimg_ptr[idx].b = inimg.at<Vec3b>(y, x)[0];
			outimg_ptr[idx].g = inimg.at<Vec3b>(y, x)[1];
			outimg_ptr[idx].r = inimg.at<Vec3b>(y, x)[2];
		}
}
int cvp::slic_int_to_mat(const gSLICr::IntImage* inimg, cv::Mat& outimg)
{
	const int* inimg_ptr = inimg->GetData(MEMORYDEVICE_CPU);
	int n = outimg.total();
	int* p = outimg.ptr<int>(0);
	unordered_map<int, int> map;
	for (int i = 0; i < n; i++)
	{
		int src_idx = inimg_ptr[i];
		if (!map.count(src_idx))
			map[src_idx] = map.size();
		p[i] = map.at(src_idx);
	}
	return map.size();
}
#endif // USE_GPU_SLIC

cv::Mat cvp::visualize_superpixel(const cv::Mat& sp)
{
	unordered_map<int, Vec3b> map;
	Mat visseg(sp.size(), CV_8UC3, Scalar::all(0));
	int* px = (int*)sp.ptr(0);
	Vec3b* pvis = visseg.ptr<Vec3b>(0);
	for (int i = 0; i < sp.total(); i++)
	{
		int lb = px[i];
		if (!map.count(lb))
			map[lb] = Vec3b(rand() % 255, rand() % 255, rand() % 255);
		pvis[i] = map[lb];
	}
	return visseg;
}

bool dfs_remove_small_region(Mat rst, int i, int j, int idx, Mat label, int cnt, int& sz)
{
	if (i < 0 || j < 0 || i >= rst.rows || j >= rst.cols || rst.at<uchar>(i, j) != idx)
		return false;
	queue<Point> Q;
	Q.push(Point(j, i));
	rst.at<uchar>(i, j) = 255;
	label.at<int>(i, j) = cnt;
	int n1 = 1, n2 = 0;
	while (!Q.empty())
	{
		Point p0 = Q.front();
		Q.pop();
		n1--;
		sz++;
		int x = p0.x, y = p0.y;
		Point pr(x + 1, y), pl(x - 1, y), pu(x, y - 1), pd(x, y + 1);
		if (pl.x >= 0 && rst.at<uchar>(pl) == idx)
		{
			Q.push(pl);
			rst.at<uchar>(pl) = 255;
			label.at<int>(pl) = cnt;
			n2++;
		}
		if (pr.x < rst.cols && rst.at<uchar>(pr) == idx)
		{
			Q.push(pr);
			rst.at<uchar>(pr) = 255;
			label.at<int>(pr) = cnt;
			n2++;
		}
		if (pu.y >= 0 && rst.at<uchar>(pu) == idx)
		{
			Q.push(pu);
			rst.at<uchar>(pu) = 255;
			label.at<int>(pu) = cnt;
			n2++;
		}
		if (pd.y < rst.rows && rst.at<uchar>(pd) == idx)
		{
			Q.push(pd);
			rst.at<uchar>(pd) = 255;
			label.at<int>(pd) = cnt;
			n2++;
		}
		if (n1 == 0)
			swap(n1, n2);
	}
	return true;
}

void dfs_find_nbs(Mat label, int i, int j, int x, unordered_set<int>& indices)
{
	if (i < 0 || j < 0 || i >= label.rows || j >= label.cols)
		return;
	int lb = label.at<int>(i, j);
	if (lb == -1)
		return;
	if (lb != x)
	{
		indices.insert(lb);
		return;
	}
	label.at<int>(i, j) = -1;
	int offset[4][2] = { { 0, 1 },{ 0, -1 },{ -1, 0 },{ 1, 0 } };
	for (int k = 0; k < 4; k++)
		dfs_find_nbs(label, i + offset[k][0], j + offset[k][1], x, indices);
	label.at<int>(i, j) = lb;
}

void dfs_fill_region(Mat backup, Mat rst, int i, int j, int _lb, int filled_lb)
{
	if (i < 0 || j < 0 || i >= rst.rows || j >= rst.cols || backup.at<uchar>(i, j) != _lb)
		return;
	backup.at<uchar>(i, j) = filled_lb;
	rst.at<uchar>(i, j) = filled_lb;
	int offset[4][2] = { { 0, 1 },{ 0, -1 },{ -1, 0 },{ 1, 0 } };
	for (int k = 0; k < 4; k++)
		dfs_fill_region(backup, rst, i + offset[k][0], j + offset[k][1], _lb, filled_lb);
	backup.at<uchar>(i, j) = _lb;
}

void cvp::remove_small_region(cv::Mat& rst, int tol)
{
	int cnt = 0;
	unordered_map<int, pair<int, Point>> map;
	Mat backup = rst.clone();
	Mat label(rst.size(), CV_32SC1);
	for (int i = 0; i < rst.rows; i++)
	{
		uchar* p = rst.ptr(i);
		for (int j = 0; j < rst.cols; j++)
		{
			int sz = 0;
			if (p[j] == 255)
				continue;
			bool b = dfs_remove_small_region(rst, i, j, p[j], label, cnt, sz);
			if (b)
			{
				map[cnt] = { sz, Point(j, i) };
				cnt++;
			}
		}
	}
	for (auto it = map.begin(); it != map.end(); it++)
	{
		if (it->second.first > tol)
			continue;
		unordered_set<int> indices;
		dfs_find_nbs(label, it->second.second.y, it->second.second.x, it->first, indices);
		int maxSz = 0, maxSzIdx = -1;
		for (auto it = indices.begin(); it != indices.end(); it++)
		{
			if (map[*it].first > maxSz)
			{
				maxSzIdx = *it;
				maxSz = map[*it].first;
			}
		}
		Point pxy = map[maxSzIdx].second;
		uchar filled_lb = backup.at<uchar>(pxy);
		uchar _lb = backup.at<uchar>(it->second.second);
		dfs_fill_region(backup, rst, it->second.second.y, it->second.second.x, _lb, filled_lb);
	}
	backup.copyTo(rst, rst == 255);
}

