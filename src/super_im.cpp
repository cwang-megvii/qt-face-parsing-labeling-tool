#include "super_im.h"

using namespace std;
using namespace cv;

SuperIm::SuperIm(const cv::Mat& _im, const std::string& _fname)
{
	im = _im;
	supim_index = Mat(im.size(), CV_32SC1);
	supim_color = Mat(im.size(), im.type());
	fname = _fname;
}

void SuperIm::compute_superpixel(int num_sps)
{
	float t1, t2;
	t1 = utils::time();
	compute_supim_index(num_sps);
	t2 = utils::time();
	cout << "compute super-pixels: " << t2 - t1 << endl;
	compute_suptb_color();
	compute_supim_color();
}

void SuperIm::set_mlbmask(const cv::Mat& mlbmask)
{
	compute_suptb_label(mlbmask);
	ukn_index_map.clear(); ukn_index_map.resize(num_superpixels, -1);
	ukn_index_map_reverse.clear(); ukn_index_map_reverse.resize(num_superpixels, -1);
	std::unordered_map<seg::label_type, cv::Mat> color_map;
	unordered_map<seg::label_type, int> cnts;
	for (int i = 0; i < num_superpixels; i++)
		cnts[suptb_label[i]]++;
	for (auto it = cnts.begin(); it != cnts.end(); it++)
	{
		color_map[it->first] = Mat(it->second, 3, CV_32FC1, Scalar(0.0));
		it->second = 0;
	}
	for (int i = 0; i < num_superpixels; i++)
	{
		seg::label_type lb = suptb_label[i];
		color_map[lb].at<Vec3f>(cnts[lb], 0) = suptb_color[i];
		ukn_index_map[i] = lb == seg::FGD_UKN ? cnts[lb] : -1;
		if (lb == seg::FGD_UKN)
			ukn_index_map_reverse[cnts[lb]] = i;
		cnts[lb]++;
	}
	suptb_color_akn_map.resize(seg::NUM_LABEL_TYPE);
	for (auto it = color_map.begin(); it != color_map.end(); it++)
	{
		if (it->first == seg::FGD_UKN)
			suptb_color_ukn = it->second;
		else
			suptb_color_akn_map[it->first] = it->second;
	}
}

void SuperIm::compute_supim_index(int num_sps)
{
	Mat dt = imread(fname, IMREAD_UNCHANGED);
	if (!dt.empty())
	{
		memcpy(supim_index.data, dt.data, sizeof(int) * supim_index.total());
		double minVal, maxVal;
		minMaxLoc(supim_index, &minVal, &maxVal);
		num_superpixels = maxVal - minVal + 1;
		return;
	}
#ifdef USE_GPU_SLIC
	cout << "run slic" << endl;
	int nPixels = im.total();
	gSLICr::objects::settings my_settings;
	my_settings.img_size.x = im.cols;
	my_settings.img_size.y = im.rows;
	my_settings.no_segs = num_sps;
	my_settings.spixel_size = std::max(5.0f, nPixels / (float)num_sps);
	my_settings.coh_weight = 0.6f;
	my_settings.no_iters = 5;
	my_settings.color_space = gSLICr::XYZ; // gSLICr::CIELAB for Lab, or gSLICr::RGB for RGB
	my_settings.seg_method = gSLICr::GIVEN_SIZE; // or gSLICr::GIVEN_NUM for given number
	my_settings.do_enforce_connectivity = true; // whether or not run the enforce connectivity step
	gSLICr::engines::core_engine gSLICr_engine(my_settings); // <=== MEMORY LEAK HERE!
	gSLICr::UChar4Image in_img(my_settings.img_size, true, true);
	cvp::mat_to_slic_uchar4(im, &in_img);
	gSLICr_engine.Process_Frame(&in_img);
	const gSLICr::IntImage* p = gSLICr_engine.Get_Seg_Res();
	in_img.Free();
	num_superpixels = cvp::slic_int_to_mat(p, supim_index);
#else
	cout << "NO GPU SLIC CODE FOUND!!! ERROR!!!" << endl;
	exit(-1);
#endif

	dt = Mat(supim_index.size(), CV_8UC4);
	memcpy(dt.data, supim_index.data, sizeof(int) * supim_index.total());
	imwrite(fname, dt);
}

void SuperIm::compute_supim_color()
{
	for (int i = 0; i < im.rows; i++)
	{
		Vec3b* p = supim_color.ptr<Vec3b>(i);
		int* psp = supim_index.ptr<int>(i);
		for (int j = 0; j < im.cols; j++)
			p[j] = suptb_color[psp[j]];
	}
}

void SuperIm::compute_suptb_color()
{
	suptb_color.resize(num_superpixels, Vec3f(0, 0, 0));
	suptb_pos.resize(num_superpixels, Point2f(0, 0));
	vector<int> cnts(num_superpixels, 0);
	for (int i = 0; i < im.rows; i++)
	{
		Vec3b* pim = im.ptr<Vec3b>(i);
		int* psp = supim_index.ptr<int>(i);
		for (int j = 0; j < im.cols; j++)
		{
			suptb_color[psp[j]] += pim[j];
			suptb_pos[psp[j]] += Point2f(j, i);
			cnts[psp[j]]++;
		}
	}
	for (int i = 0; i < suptb_color.size(); i++)
	{
		suptb_color[i] /= (cnts[i] + 1e-5);
		suptb_pos[i] /= (cnts[i] + 1e-5);
	}
}

void SuperIm::compute_suptb_label(const Mat& mlbmask)
{
	suptb_label.resize(num_superpixels, seg::FGD_UKN);
	vector<unordered_set<seg::label_type>> superpixel_lb_sets(num_superpixels);
	int* sp = supim_index.ptr<int>(0);
	for (int i = 0; i < supim_index.total(); i++)
	{
		seg::label_type lb = (seg::label_type)mlbmask.data[i];
		int spidx = sp[i];
		superpixel_lb_sets[spidx].insert(lb);
	}
	for (int i = 0; i < num_superpixels; i++)
	{
		if (superpixel_lb_sets[i].size() == 1)
			suptb_label[i] = *superpixel_lb_sets[i].begin();
	}
}
