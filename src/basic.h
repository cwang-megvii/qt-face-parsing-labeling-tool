#ifndef _BASIC_H_
#define _BASIC_H_

#include <unordered_set>
#include <unordered_map>
#include <vector>
#include <string>
#include <fstream>
#include <sstream>
#include <numeric>

namespace seg
{
	enum label_type { // BGD MUST = 0, OTHERS STARTS AT 1, FGD_UKN = 255 
		BGD = 0,
		SKIN,
		HAIR,
		CLOTH,
		NUM_LABEL_TYPE, // <=== NOT USED, ONLY FOR RECORD NUM LABELS
		SEED, // <=== For GMM 
		FGD_UKN = 255,
	};
}

#endif
