#include <qapplication.h>
#include "MainApp.h"
#include <windows.h>

using namespace std;
using namespace cv;
using namespace cv::ml;
//#pragma comment( linker, "/subsystem:\"windows\" /entry:\"mainCRTStartup\"" )

#include <algorithm>

void test()
{
	Mat im = imread("./data/input.jpg");
	Mat mask = imread("./data/mask.png", IMREAD_GRAYSCALE);
	int n = countNonZero(mask);
	Mat samples(n, 3, CV_32FC1, Scalar(0.0));
	Mat samples1(im.total() - n, 3, CV_32FC1, Scalar(0.0));
	int count = 0, count1 = 0;
	for (int i = 0; i < im.rows; i++)
	{
		for (int j = 0; j < im.cols; j++)
		{
			Vec3b c = im.at<Vec3b>(i, j);
			if (mask.at<uchar>(i, j) != 0)
			{
				samples.at<float>(count, 0) = c[0];
				samples.at<float>(count, 1) = c[1];
				samples.at<float>(count, 2) = c[2];
				count++;
			}
			else
			{
				samples1.at<float>(count1, 0) = c[0];
				samples1.at<float>(count1, 1) = c[1];
				samples1.at<float>(count1, 2) = c[2];
				count1++;
			}
		}
	}
	Ptr<cv::ml::EM> p = cv::ml::EM::create();
	p->setClustersNumber(64);
	p->trainEM(samples);
	Mat prob;
	p->predict(samples1, prob);
	prob = prob * p->getWeights().t();
	Mat out(im.size(), CV_32FC1, Scalar(0.0));
	count = 0;
	for (int i = 0; i < out.rows; i++)
		for (int j = 0; j < out.cols; j++)
		{
			if (mask.at<uchar>(i, j) == 0)
				out.at<float>(i, j) = prob.at<double>(count++);
		}
	ofstream ofile("de2.txt");
	ofile << prob << endl;
	ofile.close();
	out.convertTo(out, CV_8UC1, 255.0);
	imwrite("out.png", out);
}

void test2()
{
	const int N = 4;
	const int N1 = (int)sqrt((double)N);
	const Scalar colors[] =
	{
		Scalar(0,0,255), Scalar(0,255,0),
		Scalar(0,255,255),Scalar(255,255,0)
	};

	int i, j;
	int nsamples = 100;
	Mat samples(nsamples, 2, CV_32FC1);
	Mat labels;
	Mat img = Mat::zeros(Size(500, 500), CV_8UC3);
	Mat sample(1, 2, CV_32FC1);

	samples = samples.reshape(2, 0);
	for (i = 0; i < N; i++)
	{
		// form the training samples
		Mat samples_part = samples.rowRange(i*nsamples / N, (i + 1)*nsamples / N);

		Scalar mean(((i%N1) + 1)*img.rows / (N1 + 1),
			((i / N1) + 1)*img.rows / (N1 + 1));
		Scalar sigma(30, 30);
		randn(samples_part, mean, sigma);
	}
	samples = samples.reshape(1, 0);

	// cluster the data
	Ptr<EM> em_model = EM::create();
	em_model->setClustersNumber(N);
	em_model->setCovarianceMatrixType(EM::COV_MAT_SPHERICAL);
	em_model->setTermCriteria(TermCriteria(TermCriteria::COUNT + TermCriteria::EPS, 300, 0.1));
	em_model->trainEM(samples, noArray(), labels, noArray());

	// classify every image pixel
	for (i = 0; i < img.rows; i++)
	{
		for (j = 0; j < img.cols; j++)
		{
			sample.at<float>(0) = (float)j;
			sample.at<float>(1) = (float)i;
			int response = cvRound(em_model->predict2(sample, noArray())[1]);
			Scalar c = colors[response];

			circle(img, Point(j, i), 1, c*0.75, FILLED);
		}
	}

	//draw the clustered samples
	for (i = 0; i < nsamples; i++)
	{
		Point pt(cvRound(samples.at<float>(i, 0)), cvRound(samples.at<float>(i, 1)));
		circle(img, pt, 1, colors[labels.at<int>(i)], FILLED);
	}

	imshow("EM-clustering result", img);
	waitKey(0);
}

void test3()
{
	Mat im = imread("before.png", IMREAD_GRAYSCALE);
	cvp::remove_small_region(im, 10);
	imwrite("debug.png", im);
	exit(-1);
}

int main(int argc, char** argv)
{
	//test2();
	//test3();
	//return 0;
	QApplication app(argc, argv);
	MainApp app_ui;
	app_ui.setWindowTitle("MainWindow");
	int screen_width = GetSystemMetrics(SM_CXSCREEN);
	int screen_height = GetSystemMetrics(SM_CYSCREEN);
	app_ui.setMinimumSize(screen_width / 2, screen_height / 2);
	
	app_ui.show();
	QObject::connect(qApp, SIGNAL(lastWindowClosed()), qApp, SLOT(quit()));
	return app.exec();
}