#include "BoundaryLine.h"
#include <algorithm>

using namespace std;

BoundaryLine::BoundaryLine(const QColor &color, BoundaryPoint* _p1, BoundaryPoint* _p2, Boundary* b) :
	p1(_p1), p2(_p2), color(color), boundary(b)
{
	//setFlags(ItemIsSelectable | ItemIsMovable | ItemIgnoresTransformations);
	//setAcceptHoverEvents(true);
	//setCursor(Qt::OpenHandCursor);
}

QRectF BoundaryLine::boundingRect() const
{
	//QPointF pos1 = p1->center_pos(), pos2 = p2->center_pos();
	//QPointF m1(min(pos1.x(), pos2.x()), min(pos1.y(), pos2.y()));
	//QPointF m2(max(pos1.x(), pos2.x()), max(pos1.y(), pos2.y()));
	//return QRectF(m1 - QPointF(2,2), m2 + QPointF(2,2));
	return boundary->rect();
}

void BoundaryLine::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
	Q_UNUSED(widget);

	QTransform t = painter->transform();
	qreal m11 = t.m11(), m22 = t.m22();

	QColor fillColor = (option->state & QStyle::State_Selected) ? color.dark(150) : color;
	if (option->state & QStyle::State_MouseOver)
		fillColor = fillColor.light(125);

	QPen pen = painter->pen();
	float width = init_width;
	if (option->state & QStyle::State_Selected)
		width += 2;
	width /= m11;

	pen.setWidthF(width);
	pen.setColor(QColor(0, 255, 255, 192));
	pen.setStyle(Qt::DashLine);
	QBrush b(QColor(255, 0, 0));//painter->brush();
	painter->setBrush(QBrush(fillColor.dark(option->state & QStyle::State_Sunken ? 120 : 100)));
	painter->setPen(pen);

	painter->drawLine(p1->pos(), p2->pos());
	painter->setBrush(b);

	// Draw text
	//int fontsize = 14;
	//QFont font("Consolas", fontsize);
	//font.setStyleStrategy(QFont::ForceOutline);
	//painter->setFont(font);
	//painter->save();//
	//painter->drawText(width_val / 2 - fontsize * 3 / 4, height_val / 2 + fontsize / 2, QString("%1").arg(m_id, 2, 10, QLatin1Char('0')));
}
